//
//  WeightsType.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

enum WeightsType: String {
  case cargos, passengers
}
