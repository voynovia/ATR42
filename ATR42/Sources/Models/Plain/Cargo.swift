//
//  Cargo.swift
//  WaB
//
//  Created by Igor Voynov on 25.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public struct Cargo {
  public let sta: Double
  public let maxWeight: Double
  public var weight: Double
  
  public init(sta: Double, maxWeight: Double, weight: Double) {
    self.sta = sta
    self.maxWeight = maxWeight
    self.weight = weight
  }
}
