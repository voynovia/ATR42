//
//  AircraftExample.swift
//  WaBExample
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

class AircraftExample {
  
  var aircraft: AircraftEntity
  
  init() {
    aircraft = AircraftEntity()
  }
}
