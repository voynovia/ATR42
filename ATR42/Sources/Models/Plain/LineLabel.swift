//
//  LineLabel.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

enum LineLabel: String {
  case mtow, mlw, mzfw
  
  var name: String {
    return rawValue.uppercased()
  }
  
  var text: String {
    switch self {
    case .mtow:
      return "Take-Off Check"
    case .mlw:
      return "Landing Check"
    case .mzfw:
      return "Zero Fuel Check"
    }
  }
  
  var badge: UILabel {
    let label = UILabel()
    switch self {
    case .mtow:
      label.text = "2"
    case .mlw:
      label.text = "3"
    case .mzfw:
      label.text = "1"
    }
    label.textColor = .darkGray
    label.backgroundColor = .white
    label.font = GraphSettings.fontWarnings
    label.textAlignment = .center
    let width = label.intrinsicContentSize.width
    let height = label.intrinsicContentSize.height
    let size = width > height ? width : height
    label.frame = CGRect(x: 0, y: 0, width: size, height: size)
    label.layer.borderColor = UIColor.darkGray.cgColor
    label.layer.borderWidth = 2
    label.layer.masksToBounds = true
    label.layer.cornerRadius = size / 2
    return label
  }
  
  var textColor: UIColor {
    switch self {
    case .mtow:
      return .black
    case .mlw:
      return .black
    case .mzfw:
      return .black
    }
  }
  
  var backgroundColor: UIColor {
    switch self {
    case .mtow:
      return UIColor(.backgroundGraph)
    case .mlw:
      return .white
    case .mzfw:
      return .white
    }
  }
  
  var upPosition: Bool {
    switch self {
    case .mtow:
      return true
    case .mlw:
      return false
    case .mzfw:
      return false
    }
  }
  
}
