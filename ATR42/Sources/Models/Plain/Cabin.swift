//
//  Cabin.swift
//  WaB
//
//  Created by Igor Voynov on 25.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public class Cabin {
  public let name: String
  public let sta: Double
  public let countMax: Int
  public var passengers: [Passenger]
  
  public init(name: String, sta: Double, countMax: Int, passengers: [Passenger]) {
    self.name = name
    self.sta = sta
    self.countMax = countMax
    self.passengers = passengers
  }
}
