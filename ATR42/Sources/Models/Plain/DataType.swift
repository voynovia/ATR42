//
//  DataType.swift
//  WaBExample
//
//  Created by Igor Voynov on 13.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

enum DataType: String {
  case cargo, cabin, luggage
  
  static let values: [DataType] = [cargo, cabin, luggage]
}
