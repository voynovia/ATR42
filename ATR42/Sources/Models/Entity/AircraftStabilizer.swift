//
//  StabilizerTrim.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftStabilizer: Object, Unmarshaling {
  @objc dynamic var mac: Int = 0
  @objc dynamic var weight: Double = 0
  
  @objc dynamic var uuid: String = ""
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    mac = try json <| "mac"
    weight = try json <| "weight"
    uuid = "\(mac)/\(weight)"
  }
}
