//
//  Modification.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftTail: Object, Unmarshaling {
  
  @objc dynamic var uuid: String = ""
  @objc dynamic var name: String = ""
  @objc dynamic var version: String = ""
  @objc dynamic var taxiWeight: Double = 0
  @objc dynamic var takeOffWeight: Double = 0
  @objc dynamic var landingWeight: Double = 0
  @objc dynamic var zeroFuelWeight: Double = 0
  @objc dynamic var emptyWeight: Double = 0
  @objc dynamic var emptyIndex: Double = 0
  
  @objc dynamic var secondAttendantSeatPerWeight: Double = 0
  
  @objc dynamic var aircraft: AircraftEntity?
  
  var cabins = List<AircraftCabin>()
  var cargos = List<AircraftCargo>()
  var dods = List<AircraftDod>()
  
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    uuid = try json <| "uuid"
    name = try json <| "tailNumber"
    
    let typeUUID: String = try json <| "typeUUID"
    aircraft = StorageController().fetch(AircraftEntity.self, forPrimaryKey: typeUUID)
    
    version = try json <| "wab.version"
    taxiWeight = try json <| "wab.taxiWeight"
    takeOffWeight = try json <| "wab.takeOffWeight"
    landingWeight = try json <| "wab.landingWeight"
    zeroFuelWeight = try json <| "wab.zeroFuelWeight"
    emptyWeight = try json <| "wab.emptyWeight"
    emptyIndex = try json <| "wab.emptyIndex"
    
    secondAttendantSeatPerWeight = try json <| "wab.secondAttendantSeatPerWeight"
    
    cabins = json <| "wab.cabins"
    cargos = json <| "wab.cargo"
    dods = json <| "wab.dods"
  }
}
