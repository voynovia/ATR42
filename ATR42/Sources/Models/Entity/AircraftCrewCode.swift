//
//  CrewCode.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftCrewCode: Object, Unmarshaling {

  @objc dynamic var cockpit: Int = 0
  @objc dynamic var cabin: Int = 0
  
  @objc dynamic var uuid: String = ""
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  convenience init(cockpit: Int, cabin: Int) {
    self.init()
    self.cockpit = cockpit
    self.cabin = cabin
    uuid = "\(cockpit)/\(cabin)"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    cockpit = try json <| "cockpit"
    cabin = try json <| "cabin"
    uuid = "\(cockpit)/\(cabin)"
  }
}
