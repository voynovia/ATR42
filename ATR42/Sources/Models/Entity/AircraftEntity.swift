//
//  Aircraft.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftEntity: Object, Unmarshaling {
  
  @objc dynamic var uuid: String = UUID().uuidString
  @objc dynamic var name: String = ""
  @objc dynamic var mac: Double = 0
  @objc dynamic var refSta: Double = 0
  @objc dynamic var lemac: Double = 0
  @objc dynamic var stabMin: AircraftStabilizer!
  @objc dynamic var stabMax: AircraftStabilizer!
  @objc dynamic var allowSecondAttendant: Bool = false
  @objc dynamic var secondAttendantWeight: Double = 0
  
  var boxes = List<AircraftBox>()
  var passengers = List<AircraftPassenger>()
  var fuelEffects = List<AircraftWeightIndex>()

  let tails = LinkingObjects(fromType: AircraftTail.self, property: "aircraft")
  
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    uuid = try json <| "uuid"
    name = try json <| "model"
    mac = try json <| "wab.mac"
    refSta = try json <| "wab.refSta"
    lemac = try json <| "wab.lemac"
    boxes = json <| "wab.boxes"
    stabMin = try json <| "wab.stabilizerMin"
    stabMax = try json <| "wab.stabilizerMax"
    passengers = json <| "wab.passengers"
    fuelEffects = json <| "wab.fuelEffects"
    allowSecondAttendant = try json <| "wab.allowSecondAttendant"
    secondAttendantWeight = try json <| "wab.secondAttendantWeight"
  }
}
