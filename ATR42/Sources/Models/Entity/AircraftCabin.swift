//
//  AircraftCabin.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftCabin: Object, Unmarshaling {
  
  @objc dynamic var name: String = ""
  @objc dynamic var sta: Double = 0
  @objc dynamic var max: Int = 0
  
  @objc dynamic var uuid: String = ""
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    name = try json <| "name"
    sta = try json <| "sta"
    max = try json <| "max"
    uuid = "\(name)/\(sta)/\(max)"
  }
}
