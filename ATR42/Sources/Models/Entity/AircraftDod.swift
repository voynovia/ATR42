//
//  Dod.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftDod: Object, Unmarshaling {
  
  @objc dynamic var pantry: AircraftPantryCode?
  @objc dynamic var crew: AircraftCrewCode?
  @objc dynamic var dow: Double = 0
  @objc dynamic var doi: Double = 0
  
  convenience init(pantry: AircraftPantryCode, crew: AircraftCrewCode, dow: Double, doi: Double) {
    self.init()
    self.pantry = pantry
    self.crew = crew
    self.dow = dow
    self.doi = doi
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    pantry = try json <| "pantry"
    crew = try json <| "crew"
    dow = try json <| "dow"
    doi = try json <| "doi"
  }
}
