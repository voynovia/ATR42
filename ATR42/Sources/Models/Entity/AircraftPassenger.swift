//
//  AircraftPassenger.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftPassenger: Object, Unmarshaling {
  
  @objc dynamic var name: String = ""
  @objc dynamic var weightSummer: Double = 0
  @objc dynamic var weightWinter: Double = 0
  
  @objc dynamic var uuid: String = ""
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    name = try json <| "name"
    weightSummer = try json <| "weightSummer"
    weightWinter = try json <| "weightWinter"
    uuid = "\(name)/\(weightSummer)/\(weightWinter)"
  }
}
