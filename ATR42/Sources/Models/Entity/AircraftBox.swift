//
//  GraphLine.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftBox: Object, Unmarshaling {
  
  @objc dynamic var uuid: String = ""
  @objc dynamic var type: String = ""
  @objc dynamic var desc: String?
  
  var forward = List<AircraftWeightIndex>()
  var aft = List<AircraftWeightIndex>()
    
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    uuid = try json <| "uuid"
    type = try json <| "type"
    desc = try? json <| "description"
    forward = json <| "forward"
    aft = json <| "aft"
  }
}
