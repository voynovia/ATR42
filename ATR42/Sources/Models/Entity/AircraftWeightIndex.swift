//
//  GraphPoint.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Marshal
import RealmSwift

class AircraftWeightIndex: Object, Unmarshaling {
  
  @objc dynamic var weight: Double = 0
  @objc dynamic var index: Double = 0
  
  @objc dynamic var uuid: String = ""
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  required convenience init(object json: MarshaledObject) throws {
    self.init()
    weight = try json <| "weight"
    index = try json <| "index"
    uuid = "\(weight)/\(index)"
  }
}
