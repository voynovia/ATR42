//
//  MacScaleView.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class MacScaleView: UIView {
  
  public var minValue: Int = Int.min
  public var maxValue: Int = Int.max
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    let values = [Int](minValue-1...maxValue+1)
    var index = 0
    var sign: Bool = false
    let step = rect.width / CGFloat(maxValue - minValue + 2)
    for x in stride(from: 0, through: rect.maxX, by: step) {
      
      let value = values[index]
      if sign && value >= minValue && value <= maxValue {
        let label = UILabel(frame: CGRect(x: x-(step/2), y: 0, width: step, height: rect.height))
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = GraphSettings.fontScale
        label.text = String(value)
        addSubview(label)
      }
      sign = !sign
      index += 1
      
      let line = UIBezierPath()
      line.move(to: CGPoint(x: x, y: rect.height))
      line.addLine(to: CGPoint(x: x, y: rect.height - GraphSettings.dashSmall))
      UIColor.lightGray.setStroke()
      line.stroke()
    }
  }
}
