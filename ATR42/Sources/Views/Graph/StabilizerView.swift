//
//  StabilizerView.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class StabilizerView: UIView {
  
  let min: AircraftStabilizer
  let max: AircraftStabilizer
  
  private let multiple: Double = 0.5
  private var start: CGFloat {
    return bounds.minX + 70
  }
  private var end: CGFloat {
    return bounds.maxX - 20
  }
  private var xZero: CGFloat = 0
  private var yZero: CGFloat = 40
  private var macStep: CGFloat {
    return (end - start) / CGFloat(max.mac-min.mac)
  }
  private var weightStep: CGFloat = 0
  private var dash: UIView!
  private var label: UILabel!
  
  init(min: AircraftStabilizer, max: AircraftStabilizer, frame: CGRect) {
    self.min = min
    self.max = max
    super.init(frame: frame)
    self.backgroundColor = .clear
    layer.borderWidth = 1
    layer.borderColor = UIColor.black.cgColor
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setLabel(frame: CGRect, text: String) {
    let label = UILabel(frame: frame)
    label.textAlignment = .center
    label.textColor = .darkGray
    label.font = GraphSettings.fontScale
    label.text = text
    addSubview(label)
  }
  
  private var desc: UILabel {
    let description = UILabel()
    description.text = "← UP ↔︎ DOWN →"
    description.textAlignment = .center
    description.textColor = .darkGray
    description.frame.origin = CGPoint(x: xZero - description.intrinsicContentSize.width / 2, y: 70)
    description.frame.size = description.intrinsicContentSize
    description.font = GraphSettings.fontStabilizer
    return description
  }
  
  public func setValue(mac: Double, weight: Double) {
    clean()
    if Int(mac) > min.mac && Int(mac) < max.mac {
      let x = (CGFloat(mac) - CGFloat(min.mac)) * macStep
      let width: CGFloat = 4
      let height: CGFloat = 20
      dash = UIView(frame: CGRect(x: start + x - width, y: yZero - height/2, width: width, height: height))
      dash.backgroundColor = GraphSettings.goodColor
      addSubview(dash)
      
      label = UILabel()
      label.textColor = GraphSettings.goodColor
      label.backgroundColor = .white
      label.layer.borderColor = GraphSettings.goodColor.cgColor
      label.layer.borderWidth = 1
      label.text = " \(weight.rounded(toPlaces: 2)) "
      label.frame.origin = CGPoint(x: dash.frame.minX - label.intrinsicContentSize.width/2, y: yZero + height/2)
      label.frame.size = label.intrinsicContentSize
      addSubview(label)
    }
  }
  
  private func drawUpScale(path: UIBezierPath) {
    let macRange = (min.mac...max.mac)
    let macValues = Array(macRange)
    var index: Int = 0
    for x in stride(from: start, through: end, by: macStep) {
      var big = (index + 5) % 5 == 0
      if x == end {
        big = true
      }
      let y: CGFloat = big ? GraphSettings.dashBig : GraphSettings.dashSmall
      if big {
        let value = macValues[index]
        setLabel(frame: CGRect(x: x-(macStep/2), y: y - 20, width: macStep, height: 20),
                 text: String(abs(value)))
      }
      path.move(to: CGPoint(x: x, y: yZero))
      path.addLine(to: CGPoint(x: x, y: yZero - y))
      index += 1
    }
  }
  
  private func drawDownScale(path: UIBezierPath) {
    xZero = (end-start)/2 + start
    let startArea = xZero - (frame.width / 3) / 2
    let endArea = xZero + (frame.width / 3) / 2
    let weightRange = (Int(max.weight * 10)...Int(min.weight * 10))
    let weightValues: [Double] = Array(weightRange).flatMap { value -> Double? in
      let newValue = Double(value) / 10
      return abs(newValue.truncatingRemainder(dividingBy: multiple)) == 0 ? newValue : nil
      }.reversed()
    weightStep = (end - start) / (CGFloat(abs(max.weight)+abs(min.weight)) / CGFloat(multiple))
    var index: Int = 0
    for x in stride(from: start, through: end, by: weightStep) {
      let y = yZero + GraphSettings.dashSmall
      let value = abs(weightValues[index])
      let integer = value.truncatingRemainder(dividingBy: 1) == 0
      if x == start || x == end || integer || (x > startArea && x < endArea) {
        setLabel(frame: CGRect(x: x-(macStep/2), y: y + 5, width: macStep, height: 20),
                 text: integer ? String(Int(value)) : String(value))
      }
      path.move(to: CGPoint(x: x, y: yZero))
      path.addLine(to: CGPoint(x: x, y: y))
      index += 1
    }
  }
  
  override func draw(_ rect: CGRect) {
    
    let label = UILabel()
    label.text = "% MAC"
    label.textColor = .darkGray
    label.font = GraphSettings.fontStabilizer
    label.frame.origin = CGPoint(x: 10, y: 15)
    label.frame.size = label.intrinsicContentSize
    addSubview(label)
    
    let scale = UIBezierPath()
    scale.move(to: CGPoint(x: start, y: yZero))
    scale.addLine(to: CGPoint(x: end, y: yZero))
    
    drawUpScale(path: scale)
    drawDownScale(path: scale)
    
    scale.lineWidth = 1
    UIColor.lightGray.setStroke()
    scale.stroke()
  
    addSubview(desc)
  }
  
  public func clean() {
    dash?.removeFromSuperview()
    label?.removeFromSuperview()
  }
  
}
