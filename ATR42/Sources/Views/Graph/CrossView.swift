//
//  CrossView.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class CrossView: UIView {
  
  let color: UIColor
  
  init(color: UIColor, frame: CGRect) {
    self.color = color
    super.init(frame: frame)
    self.backgroundColor = .clear
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    let cross = UIBezierPath()
    cross.move(to: CGPoint(x: rect.minX, y: rect.midY))
    cross.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
    cross.move(to: CGPoint(x: rect.midX, y: rect.minY))
    cross.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
    cross.lineWidth = 3
    color.setStroke()
    cross.stroke()
  }
}
