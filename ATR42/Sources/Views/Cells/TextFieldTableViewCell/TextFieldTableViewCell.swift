//
//  TextFieldTableViewCell.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var textField: UITextField!
  @IBAction func textFieldChanged(_ sender: UITextField) {
    valueChanged?(sender.text ?? "")
  }
  
  var valueChanged: ((String) -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    
    titleLabel.textColor = .lightGray
    
    textField.returnKeyType = .done
    textField.delegate = self
    textField.clearsOnBeginEditing = true
    textField.inputAssistantItem.leadingBarButtonGroups.removeAll()
    textField.inputAssistantItem.trailingBarButtonGroups.removeAll()
    textField.autocapitalizationType = .allCharacters
  }
}

extension TextFieldTableViewCell: UITextFieldDelegate {
  private var doneToolBar: UIToolbar {
    let barFrame = CGRect(x: 0, y: 0, width: 0, height: 40)
    let bar = UIToolbar(frame: barFrame)
    bar.barTintColor = .darkGray
    bar.tintColor = .white
    bar.isTranslucent = false
    let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(endEditing))
    let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    bar.items = [flex, btnDone]
    return bar
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if UIDevice.current.userInterfaceIdiom == .phone {
      switch textField.keyboardType {
      case .numberPad, .decimalPad:
        textField.inputAccessoryView = doneToolBar
      default:
        textField.inputAccessoryView = nil
      }
    }
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.endEditing(true)
    return true
  }
  func textFieldDidEndEditing(_ textField: UITextField) {
    guard let text = textField.text, !text.isEmpty else { return }
    valueChanged?(text)
  }
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    switch textField.keyboardType {
    case .asciiCapable:
      let characterSet = CharacterSet(charactersIn: string)
      return CharacterSet.letters.isSuperset(of: characterSet)
    case .phonePad:
      var allow = CharacterSet.decimalDigits
      allow.insert(charactersIn: "+")
      allow.insert(charactersIn: "*")
      allow.insert(charactersIn: "#")
      let characterSet = CharacterSet(charactersIn: string)
      return allow.isSuperset(of: characterSet)
    case .decimalPad:
      var allow = CharacterSet.decimalDigits
      allow.insert(charactersIn: ",")
      allow.insert(charactersIn: ".")
      let characterSet = CharacterSet(charactersIn: string)
      return allow.isSuperset(of: characterSet)
    default:
      return true
    }
  }
}
