//
//  SelectTableViewCell.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class SelectTableViewCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  
  var cellSelected: (() -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    titleLabel.textColor = .lightGray
    valueLabel.textColor = .white
    accessoryType = .disclosureIndicator
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    if selected {
      cellSelected?()
    }
  }
  
}
