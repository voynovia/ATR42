//
//  SelectTableViewCellModel.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

class SelectTableViewCellModel {
  let title: String
  var value: String?
  var selected: () -> Void = {}
  init(title: String, value: String? = nil, selected: @escaping (() -> Void)) {
    self.title = title
    self.value = value
    self.selected = selected
  }
}

extension SelectTableViewCellModel: CellViewModelType {
  func setup(on cell: SelectTableViewCell) {
    cell.selectionStyle = .default
    
    cell.titleLabel.text = title
    cell.valueLabel.text = value
    cell.cellSelected = {
      self.selected()
    }
  }
}
