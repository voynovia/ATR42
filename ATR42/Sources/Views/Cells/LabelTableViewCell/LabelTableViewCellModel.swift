//
//  LabelTableViewCellModel.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

struct LabelTableViewCellModel {
  let title: String
  let description: String
  let descriptionColor: UIColor
}

extension LabelTableViewCellModel: CellViewModelType {
  func setup(on cell: LabelTableViewCell) {
    cell.selectionStyle = .gray
    
    cell.titleLabel.text = title
    cell.descriptionLabel.text = description
    cell.descriptionLabel.textColor = descriptionColor
  }
}
