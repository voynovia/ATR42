//
//  TextFieldTableViewCell.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TextFieldBigTableViewCell: UITableViewCell {
  
  @IBOutlet weak var textField: UITextField! {
    didSet {
      textField.returnKeyType = .done
      textField.autocorrectionType = .no
      textField.delegate = self
      textField.clearsOnBeginEditing = true
      textField.inputAssistantItem.leadingBarButtonGroups.removeAll()
      textField.inputAssistantItem.trailingBarButtonGroups.removeAll()
    }
  }
  
  @IBAction func textFieldChanged(_ sender: UITextField) {
    valueChanged?(sender.text ?? "")
  }
  
  var valueChanged: ((String) -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    backgroundColor = .clear
  }
}

extension TextFieldBigTableViewCell: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.endEditing(true)
    return true
  }
  func textFieldDidEndEditing(_ textField: UITextField) {
    guard let text = textField.text, !text.isEmpty else { return }
    valueChanged?(text)
  }
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    switch textField.keyboardType {
    case .asciiCapable:
      let characterSet = CharacterSet(charactersIn: string)
      return CharacterSet.letters.isSuperset(of: characterSet)
    case .phonePad:
      var allow = CharacterSet.decimalDigits
      allow.insert(charactersIn: "+")
      allow.insert(charactersIn: "*")
      allow.insert(charactersIn: "#")
      let characterSet = CharacterSet(charactersIn: string)
      return allow.isSuperset(of: characterSet)
    case .decimalPad:
      var allow = CharacterSet.decimalDigits
      allow.insert(charactersIn: ",")
      allow.insert(charactersIn: ".")
      let characterSet = CharacterSet(charactersIn: string)
      return allow.isSuperset(of: characterSet)
    default:
      return true
    }
  }
}
