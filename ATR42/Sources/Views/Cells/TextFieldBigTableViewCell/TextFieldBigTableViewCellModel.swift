//
//  TextFieldTableViewCellModel.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TextFieldBigTableViewCellModel {
  let placeholder: String?
  var text: String?
  let type: UIKeyboardType
  var isValid: Bool
  let valueChanged: ((String) -> Void)
  
  init(placeholder: String? = nil, text: String? = nil, isValid: Bool, type: UIKeyboardType, valueChanged: @escaping ((String) -> Void)) {
    self.placeholder = placeholder
    self.text = text
    self.type = type
    self.isValid = isValid
    self.valueChanged = valueChanged
  }
}

extension TextFieldBigTableViewCellModel: CellViewModelType {
  func setup(on cell: TextFieldBigTableViewCell) {
    cell.textField.placeholder = placeholder
    cell.textField.text = text
    cell.textField.keyboardType = type
    cell.valueChanged = { text in
      self.valueChanged(text)
    }
  }
}
