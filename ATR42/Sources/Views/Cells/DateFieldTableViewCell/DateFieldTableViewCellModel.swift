//
//  DateFieldTableViewCellModel.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class DateFieldTableViewCellModel {
  let title: String
  let mode: UIDatePickerMode
  var value: String?
  let valueChanged: (String) -> Void
  
  init(title: String, mode: UIDatePickerMode, value: String? = nil,
       valueChanged: @escaping (String) -> Void) {
    self.title = title
    self.mode = mode
    self.value = value
    self.valueChanged = valueChanged
  }
}

extension DateFieldTableViewCellModel: CellViewModelType {
  func setup(on cell: DateFieldTableViewCell) {
    cell.titleLabel.text = title
    cell.datePickerMode = mode

    cell.dateField.text = value
//    if let date = value {
//      cell.setDate(date)
//    }
    
    cell.valueChanged = { string in
      self.valueChanged(string)
    }
  }
}
