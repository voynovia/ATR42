//
//  DateFieldTableViewCell.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class DateFieldTableViewCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var dateField: UITextField!
  
  private var datePicker: UIDatePicker!
  
  var valueChanged: ((String) -> Void)?
  
  var datePickerMode: UIDatePickerMode = .date {
    didSet {
      datePicker.datePickerMode = datePickerMode
    }
  }
  
  func setDate(_ date: Date) {
    datePicker.setDate(date, animated: false)
    valueCanged(datePicker)
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    
    titleLabel.textColor = .lightGray
    
    dateField.inputAssistantItem.leadingBarButtonGroups.removeAll()
    dateField.inputAssistantItem.trailingBarButtonGroups.removeAll()
    dateField.keyboardAppearance = .default
    
    datePicker = UIDatePicker()
    datePicker.date = Date()
    datePicker.addTarget(self, action: #selector(valueCanged(_:)), for: .valueChanged)
    datePicker.setDate(Date(), animated: false)
    datePicker.setValue(UIColor.white, forKeyPath: "textColor")
    datePicker.backgroundColor = UIColor(.backgroundColor).withAlphaComponent(0.9)
    dateField.inputView = datePicker
    
    let toolBar = UIToolbar()
    toolBar.barTintColor = .darkGray
    toolBar.tintColor = .white
    toolBar.isTranslucent = false
    let flexButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let doneButton = UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(donePressed))
    toolBar.setItems([flexButton, doneButton], animated: false)
    toolBar.sizeToFit()
    dateField.inputAccessoryView = toolBar
  }
  
  @objc private func donePressed() {
    guard let text = dateField.text else { return }
    if text.isEmpty {
      setDate(Date())
    }
    dateField.resignFirstResponder()
  }
  
  @objc private func valueCanged(_ sender: UIDatePicker) {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    switch sender.datePickerMode {
    case .time:
      dateFormatter.dateFormat = "HHmm"
    default:
      dateFormatter.dateFormat = "ddMMMyy"
    }
    dateField.text = dateFormatter.string(from: sender.date).uppercased()
    valueChanged?(dateField.text!)
  }
    
}
