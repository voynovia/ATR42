//
//  TextViewTableViewCell.swift
//  Vista
//
//  Created by Igor Voynov on 15.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TextViewTableViewCell: UITableViewCell {
  
  @IBOutlet weak var textView: UITextView! {
    didSet {
      textView.textColor = .white
      textView.keyboardAppearance = .dark
      textView.inputAssistantItem.leadingBarButtonGroups.removeAll()
      textView.inputAssistantItem.trailingBarButtonGroups.removeAll()
      textView.delegate = self
    }
  }
  
  var valueChanged: ((String) -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = .clear
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  // Now would calculate manually
  override func systemLayoutSizeFitting(_ targetSize: CGSize,
                                        withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority,
                                        verticalFittingPriority: UILayoutPriority) -> CGSize {
    let height: CGFloat = 200
    return CGSize(width: targetSize.width, height: height)
  }
  
}

extension TextViewTableViewCell: UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    valueChanged?(textView.text)
  }
}
