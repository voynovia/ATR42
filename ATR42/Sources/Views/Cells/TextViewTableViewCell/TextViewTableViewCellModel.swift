//
//  TextViewTableViewCellModel.swift
//  Vista
//
//  Created by Igor Voynov on 15.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TextViewTableViewCellModel {
  var text: String
  var isValid: Bool
  let valueChanged: ((String) -> Void)
  
  init(text: String, isValid: Bool, valueChanged: @escaping ((String) -> Void)) {
    self.text = text
    self.isValid = isValid
    self.valueChanged = valueChanged
  }
}

extension TextViewTableViewCellModel: CellViewModelType {
  func setup(on cell: TextViewTableViewCell) {
    cell.textView.text = text
    cell.valueChanged = { text in
      self.valueChanged(text)
    }
  }
}
