//
//  MenuTableViewCellModel.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

struct MenuTableViewCellModel {
  let title: String
}

extension MenuTableViewCellModel: CellViewModelType {
  func setup(on cell: MenuTableViewCell) {
    cell.titleLabel.text = title
  }
}
