//
//  TabBarItemView.swift
//  Vista
//
//  Created by Igor Voynov on 07.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TabBarItemView: AZTabBarItemView {
  
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var titleLabel: UILabel! {
    didSet {
      let size: CGFloat = UIDevice.current.userInterfaceIdiom == .pad ? 14 : 10
      titleLabel.font = UIFont.systemFont(ofSize: size)
    }
  }
  
  override var accessibilityTitle: String {
    return titleLabel.text ?? super.accessibilityTitle
  }
  
  enum `Type` {
    case weightBalance
    case more
    
    var icon: UIImage {
      var image: UIImage
      switch self {
      case .weightBalance:
        image = #imageLiteral(resourceName: "ic_flight_wb-circle")
      case .more:
        image = #imageLiteral(resourceName: "ic_menu")
      }
      return image.imageWithTemplateRendingMode
    }
    
    var title: String {
      switch self {
      case .weightBalance:
        return "W&B"
      case .more:
        return "More"
      }
    }
  }
  
  enum State {
    case selected
    case unselected
    
    var color: UIColor {
      switch self {
      case .selected:
        return .white
      case .unselected:
        return UIColor(hexString: "9299a3")
      }
    }
  }
  
  private(set) var type: Type = .weightBalance {
    didSet {
      updateType()
    }
  }
  
  private(set) var state: State = .unselected {
    didSet {
      updateState()
    }
  }
  
  static func create(with type: Type) -> TabBarItemView {
    let cell = loadViewFromNib()
    cell.backgroundColor = UIColor(hexString: "373739")
    cell.type = type
    return cell
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    state = selected ? .selected : .unselected
  }
  
  private func updateState() {
    imageView.tintColor = state.color
    titleLabel.textColor = state.color
  }
  
  private func updateType() {
    imageView.image = type.icon
    titleLabel.text = type.title
  }
}
