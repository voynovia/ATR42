//
//  TabBarViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TabBarViewController: AZTabBarController {
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tabBar.backgroundImage = UIColor(.backgroundColor).as1ptImage()
  }
}
