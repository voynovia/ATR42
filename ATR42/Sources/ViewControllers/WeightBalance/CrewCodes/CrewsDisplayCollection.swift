//
//  CrewsDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

protocol CrewsSelectionDelegate: class {
  func selected(crew: AircraftCrewCode)
}

class CrewsDisplayCollection: DisplayCollection {
  
  weak var delegate: CrewsSelectionDelegate?
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [
      LabelTableViewCellModel.self
    ]
  }
  
  var tail: AircraftTail!
  var crews: [AircraftCrewCode] = []
  
  init(tail: AircraftTail) {
    self.tail = tail
    crews = Set(tail.dods.flatMap {$0.crew}).sorted(by: {$0.uuid < $1.uuid})
  }
  
  func numberOfRows(in section: Int) -> Int {
    return crews.count
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    let cell = crews[indexPath.row]
    return LabelTableViewCellModel(title: "\(cell.cockpit) / \(cell.cabin)", description: "", descriptionColor: .white)
  }
  
  func didSelect(indexPath: IndexPath) {
    delegate?.selected(crew: crews[indexPath.row])
  }
}
