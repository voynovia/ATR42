//
//  PassengersViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class WeightsViewController: UIViewController {
  
  public var aircraft: AircraftEntity!
  public var tail: AircraftTail!
  public var selectableType: WeightsType = .passengers
  
  public var passengers: [String: Cabin] = [:]
  public var cabinLuggage: Double = 0
  
  public var cargos: [String: Cargo] = [:]
  public var secondAttendant: Bool = false
  
  private let separator: CGFloat = 5
  private let inset: CGFloat = 10
  private let rowHeight: CGFloat = 32
  
  private var yCoord: CGFloat = 16
  
  typealias TextFieldValue = (DataType, String?, String?)
  private var textFields: [UITextField: TextFieldValue] = [:]
  
  fileprivate var passIndex: Double = 0
  fileprivate var passWeight: Double = 0
  fileprivate var cargosIndex: Double = 0
  fileprivate var cargosWeight: Double = 0
  
  typealias SelectedPassengers = (weight: Double, index: Double, passengers: [String: Cabin], luggage: Double)
  public lazy var selectedPassengers: ((SelectedPassengers) -> Void)? = nil
  
  typealias SelectedCargo = (weight: Double, index: Double, cargo: [String: Cargo])
  public lazy var selectedCargo: ((SelectedCargo) -> Void)? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(ok))
    
    switch selectableType {
    case .passengers:
      title = "Passengers"
    case .cargos:
      title = "Cargo"
    }
    
    view.backgroundColor = UIColor(.backgroundColor)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    switch selectableType {
    case .cargos:
      setCargosViews()
    case .passengers:
      setPasengersViews()
      setLuggageViews()
    }
  }
  
  @objc private func cancel() {
    dismiss(animated: true, completion: nil)
  }
  
  @objc private func ok() {
    view.endEditing(true)
    switch selectableType {
    case .passengers:
      setPassengers()
      selectedPassengers?((passWeight, passIndex, passengers, cabinLuggage))
    case .cargos:
      setCargos()
      selectedCargo?((cargosWeight, cargosIndex, cargos))
    }
    dismiss(animated: true, completion: nil)
  }
  
  private func setLabel(text: String, to stackView: UIStackView) {
    let label = UILabel()
    label.text = text
    label.textColor = .lightGray
    stackView.addArrangedSubview(label)
  }
  
  private func setTextField(text: String, placeholder: String?, params: TextFieldValue, to stackView: UIStackView) {
    let textField = UITextField()
    textField.delegate = self
    textField.text = text
    textField.textColor = .white
    textField.placeholder = placeholder
    textField.keyboardType = .numbersAndPunctuation
    textField.inputAssistantItem.leadingBarButtonGroups.removeAll()
    textField.inputAssistantItem.trailingBarButtonGroups.removeAll()
    textFields[textField] = params
    stackView.addArrangedSubview(textField)
  }
  
  private func setPasengersViews() {
    for aircraftCabin in tail.cabins {
      let identifier = aircraftCabin.name
      var cabin: Cabin!
      if let existingCabin = passengers[identifier] {
        cabin = existingCabin
      } else {
        
        var countMax = aircraftCabin.max
        if secondAttendant,
          let cabinLast = tail.cabins.sorted(by: {$0.name > $1.name}).first,
          aircraftCabin.name == cabinLast.name {
          countMax -= 1
        }
        
        cabin = Cabin(name: aircraftCabin.name,
                      sta: aircraftCabin.sta,
                      countMax: countMax,
                      passengers: aircraft.passengers.map { Passenger(name: $0.name,
                                                                      weight: Date().isSummer ? $0.weightSummer : $0.weightWinter,
                                                                      count: 0) })
        passengers[identifier] = cabin
      }
      
      let stackView = UIStackView(frame: CGRect(x: inset, y: yCoord, width: view.bounds.width - inset * 2, height: rowHeight))
      stackView.axis = .horizontal
      stackView.distribution = .fillProportionally
      stackView.alignment = .fill
      stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      stackView.spacing = separator
      setLabel(text: "Cabin \(cabin.name) (\(cabin.countMax))", to: stackView)
      for passenger in cabin.passengers {
        setTextField(text: passenger.count > 0 ? String(passenger.count) : "",
                     placeholder: passenger.name,
                     params: (DataType.cabin, cabin.name, passenger.name),
                     to: stackView)
      }
      view.addSubview(stackView)
      yCoord += stackView.frame.height + separator
    }
  }
  
  private func setLuggageViews() {
    let stackView = UIStackView(frame: CGRect(x: inset, y: yCoord, width: view.bounds.width - inset * 2, height: rowHeight))
    stackView.axis = .horizontal
    stackView.distribution = .fillEqually
    stackView.alignment = .fill
    stackView.spacing = separator
    yCoord += stackView.frame.height + separator
    setLabel(text: "Cabin Luggage", to: stackView)
    setTextField(text: cabinLuggage > 0 ? String(Int(cabinLuggage)) : "",
                 placeholder: nil,
                 params: (DataType.luggage, nil, nil),
                 to: stackView)
    view.addSubview(stackView)
  }
  
  private func setCargosViews() {
    for aircraftCargo in tail.cargos {
      var cargo: Cargo!
      if let existingCargo = cargos[aircraftCargo.name] {
        cargo = existingCargo
      } else {
        cargo = Cargo(sta: aircraftCargo.sta, maxWeight: aircraftCargo.max, weight: 0)
        cargos[aircraftCargo.name] = cargo
      }
      let stackView = UIStackView(frame: CGRect(x: inset, y: yCoord, width: view.bounds.width - inset * 2, height: rowHeight))
      stackView.axis = .horizontal
      stackView.distribution = .fillEqually
      stackView.alignment = .fill
      stackView.spacing = separator
      yCoord += stackView.frame.height + separator
      setLabel(text: "\(aircraftCargo.name) (\(Int(aircraftCargo.max)))", to: stackView)
      setTextField(text: cargo.weight > 0 ? String(Int(cargo.weight)) : "",
                   placeholder: nil,
                   params: (DataType.cargo, aircraftCargo.name, nil),
                   to: stackView)
      
      view.addSubview(stackView)
    }
  }
  
  // MARK: - Processing
  
  private func setPassengers() {
    passWeight = passengers.map {
      $0.value.passengers.map {Double($0.count) * $0.weight}.reduce(0, +)
      }.reduce(0, +) + cabinLuggage
    passIndex = passengers.map {
      WABMath.index(for: aircraft, sta: $0.value.sta)
        * ($0.value.passengers.map {Double($0.count) * $0.weight}.reduce(0, +))
      }.reduce(0, +)
  }
  
  private func setCargos() {
    cargosWeight = cargos.map {$0.value.weight}.reduce(0, +)
    cargosIndex = cargos.map { WABMath.index(for: aircraft, sta: $0.value.sta) * $0.value.weight }.reduce(0, +)
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let characterSet = CharacterSet(charactersIn: string)
    return CharacterSet.decimalDigits.isSuperset(of: characterSet)
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    guard var text = textField.text,
      let (type, value1, value2) = textFields[textField] else {
        return
    }
    if text.isEmpty {
      text = "0"
    }
    switch type {
    case .cabin:
      if let value1 = value1, let passengerName = value2, let count = Int(text),
        let cabin = passengers[value1] {
        passengers[value1]?.passengers.first(where: {$0.name == passengerName})?.count = 0
        let yetCount = cabin.passengers.filter { $0.name != "INFNT" && $0.name != "INFANT" }.map { $0.count }.reduce(0, +)
        var allCount = yetCount
        if passengerName != "INFNT" && passengerName != "INFANT" {
          allCount += count
        }
        if allCount <= cabin.countMax {
          passengers[value1]?.passengers.first(where: {$0.name == passengerName})?.count = count
        } else {
          textField.text = ""
          textField.shake()
        }
        setPassengers()
        #if DEBUG
          passengers.sorted(by: {$0.key < $1.key}).forEach { cabin in
            let index = WABMath.index(for: aircraft, sta: cabin.value.sta)
              * (cabin.value.passengers.map {Double($0.count) * $0.weight}.reduce(0, +))
            print("Cabin \(cabin.value.name)", index)
          }
        #endif
      }
    case .luggage:
      if let weight = Double(text) {
        cabinLuggage = weight
        setPassengers()
      }
    case .cargo:
      if let value1 = value1, let weight = Double(text), let cargo = cargos[value1] {
        cargos[value1]?.weight = 0
        if weight <= cargo.maxWeight {
          cargos[value1]?.weight = weight
        } else {
          textField.text = ""
          textField.shake()
        }
        setCargos()
      }
    }
  }
  
  override func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
