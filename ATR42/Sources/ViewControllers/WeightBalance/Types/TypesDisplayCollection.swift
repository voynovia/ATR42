//
//  TypesDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 07.02.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

protocol TypesSelectionDelegate: class {
  func selected(type: AircraftEntity)
}

class TypesDisplayCollection: DisplayCollection {
  
  weak var delegate: TypesSelectionDelegate?
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [
      LabelTableViewCellModel.self
    ]
  }
  
  var types: [AircraftEntity] = []
  
  init() {
    let sorted = StorageSorted(key: #keyPath(AircraftEntity.name), ascending: true)
    types = Array(StorageController().fetchObjects(AircraftEntity.self, predicates: nil, sorted: sorted))
  }
  
  func numberOfRows(in section: Int) -> Int {
    return types.count
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    let cell = types[indexPath.row]
    return LabelTableViewCellModel(title: cell.name, description: "", descriptionColor: .white)
  }
  
  func didSelect(indexPath: IndexPath) {
    delegate?.selected(type: types[indexPath.row])
  }
}
