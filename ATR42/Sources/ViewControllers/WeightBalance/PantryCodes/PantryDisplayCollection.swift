//
//  PantryDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

protocol PantrySelectionDelegate: class {
  func selected(pantry: AircraftPantryCode)
}

class PantryDisplayCollection: DisplayCollection {
  
  weak var delegate: PantrySelectionDelegate?
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [
      LabelTableViewCellModel.self
    ]
  }
  
  var pantry: [AircraftPantryCode] = []
  
  init(tail: AircraftTail, crew: AircraftCrewCode) {
    pantry = Set(tail.dods.filter {$0.crew == crew}.flatMap {$0.pantry}).sorted(by: { (first, second) -> Bool in
      if first.name.count == second.name.count {
        return first.name < second.name
      }
      return first.name.count < second.name.count
    })
  }
  
  func numberOfRows(in section: Int) -> Int {
    return pantry.count
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    let cell = pantry[indexPath.row]
    return LabelTableViewCellModel(title: cell.name, description: "", descriptionColor: .white)
  }
  
  func didSelect(indexPath: IndexPath) {
    delegate?.selected(pantry: pantry[indexPath.row])
  }
}
