//
//  TailsDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

protocol TailsSelectionDelegate: class {
  func selected(tail: AircraftTail)
}

class TailsDisplayCollection: DisplayCollection {
  
  weak var delegate: TailsSelectionDelegate?
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [
      LabelTableViewCellModel.self
    ]
  }
  
  var aircraft: AircraftEntity!
  var tails: [AircraftTail] = []
  
  init(aircraft: AircraftEntity) {
    self.aircraft = aircraft
    tails = aircraft.tails.sorted(by: {$0.name < $1.name})
  }
  
  func numberOfRows(in section: Int) -> Int {
    return tails.count
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    let cell = tails[indexPath.row]
    return LabelTableViewCellModel(title: cell.name, description: "", descriptionColor: .white)
  }
  
  func didSelect(indexPath: IndexPath) {
    delegate?.selected(tail: tails[indexPath.row])
  }
}
