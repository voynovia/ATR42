//
//  TailsViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class TailsViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView! {
    didSet {
      tableView.configure(with: .defaultConfiguration)
    }
  }
  
  fileprivate var displayCollection: TailsDisplayCollection!
  public weak var selectionDelegate: TailsSelectionDelegate?
  
  public lazy var selectedTail: ((AircraftTail) -> Void)? = nil
  
  var aircraft: AircraftEntity!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
    title = "Tails"
    
    displayCollection = TailsDisplayCollection(aircraft: aircraft)
    displayCollection.delegate = self
    
    tableView.registerNibs(from: displayCollection)
    tableView.tableFooterView = UIView()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorColor = .lightGray
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadDataImmediately()
  }
  
  @objc private func cancel() {
    dismiss(animated: true, completion: nil)
  }
}

extension TailsViewController: TailsSelectionDelegate {
  func selected(tail: AircraftTail) {
    selectedTail?(tail)
    dismiss(animated: true, completion: nil)
  }
}

extension TailsViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return displayCollection.numberOfSections
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return displayCollection.numberOfRows(in: section)
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = displayCollection.model(for: indexPath)
    let cell = tableView.dequeueReusableCell(for: indexPath, with: model)
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    displayCollection.didSelect(indexPath: indexPath)
  }
}
