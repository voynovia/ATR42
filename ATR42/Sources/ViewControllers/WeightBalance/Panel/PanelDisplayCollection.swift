//
//  PanelDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

class PanelDisplayCollection: DisplayCollection {
  
  weak var delegate: DisplayCollectionDelegate?
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [
      LabelTableViewCellModel.self
    ]
  }
  
  typealias CellType = (title: String, value: String, isGood: Bool)
  private var cells: [CellType] = []
  
  public func setHeaderCells(zPoint: Point, tPoint: Point, lPoint: Point) {
    cells =  [
      ("Zero Fuel Weight", String(zPoint.weight), zPoint.isGood),
      ("Take Off Weight", String(tPoint.weight), tPoint.isGood),
      ("Landing Weight", String(lPoint.weight), lPoint.isGood)
    ]
    delegate?.updateUI()
  }
  
  public func clean() {
    cells = [
      ("Zero Fuel Weight", "", false),
      ("Take Off Weight", "", false),
      ("Landing Weight", "", false)
    ]
    delegate?.updateUI()
  }
  
  init(delegate: DisplayCollectionDelegate?) {
    self.delegate = delegate
    clean()
  }
  
  func numberOfRows(in section: Int) -> Int {
    return cells.count
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    let cell = cells[indexPath.row]
    return LabelTableViewCellModel(title: cell.title, description: cell.value, descriptionColor: cell.isGood ? #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1) : #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
  }
}
