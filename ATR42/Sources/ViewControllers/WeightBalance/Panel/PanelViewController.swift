//
//  PanelViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class PanelViewController: UIViewController {
  
  fileprivate var displayCollection: PanelDisplayCollection!
  
  @IBOutlet weak var tableView: UITableView! {
    didSet {
      var configuration = TableViewConfiguration.default
      configuration.estimatedRowHeight = 44
      configuration.allowsSelection = false
      tableView.configure(with: .custom(configuration))
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    displayCollection = PanelDisplayCollection(delegate: self)
    tableView.registerNibs(from: displayCollection)
    tableView.tableFooterView = UIView()
    tableView.isScrollEnabled = false
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorColor = .clear
    tableView.allowsSelectionDuringEditing = true
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadDataImmediately()
  }
  
  public func setPoints(_ points: Points) {
    displayCollection.setHeaderCells(zPoint: points.zPoint, tPoint: points.tPoint, lPoint: points.lPoint)
  }
  
  public func clean() {
    displayCollection.clean()
  }
  
  override func viewDidLayoutSubviews() {
    view.addBottomBorderWithColor(.darkGray, width: 1)
  }
  
}

extension PanelViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return displayCollection.numberOfSections
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return displayCollection.numberOfRows(in: section)
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = displayCollection.model(for: indexPath)
    let cell = tableView.dequeueReusableCell(for: indexPath, with: model)
    return cell
  }
}
