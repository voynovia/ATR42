//
//  SettingsProcessing.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

typealias Points = (zPoint: Point, tPoint: Point, lPoint: Point)

class SettingsProcessor {
  
  public var graphController: GraphViewController!
  public var panelController: PanelViewController!
  
  public func clean() {
    graphController?.clean()
    panelController?.clean()
  }
  
  public func process(data: SettingsData) {
    guard let aircraft = data.type else { return }

    let processor = WABController(for: aircraft)
    
    guard let dod = data.tail?.dods.first(where: {$0.crew == data.crew && $0.pantry == data.pantry }) else { return }
    
    processor.process(doi: dod.doi, dow: dod.dow,
                      passengersIndex: data.passengersIndex, passengersWeight: data.passengersWeight,
                      cargosIndex: data.cargoIndex, cargosWeight: data.cargoWeight,
                      fuelBlock: data.takeOff, fuelTrip: data.trip) { result in
                        panelController.setPoints((result.zPoint, result.tPoint, result.lPoint))
                        graphController.tPoint = result.tPoint
                        graphController.lPoint = result.lPoint
                        graphController.zPoint = result.zPoint
                        graphController.stabValue = result.stabValue
                        graphController.update()
                        guard let tail = data.tail, let crew = data.crew, let pantry = data.pantry else { return }
                        graphController.printValues = PrintValues(flight: data.flightNo,
                                                                  date: data.date, time: data.time,
                                                                  from: data.from, to: data.to,
                                                                  tail: tail.name, crew: "\(crew.cockpit) / \(crew.cabin)", pantry: pantry.name,
                                                                  baseIndex: tail.emptyIndex,
                                                                  zfwMax: tail.zeroFuelWeight,
                                                                  towMax: tail.takeOffWeight,
                                                                  lwMax: tail.landingWeight,
                                                                  version: tail.version,
                                                                  dow: dod.dow, doi: dod.doi,
                                                                  takeOffFuel: data.takeOff,
                                                                  tripFuel: data.trip,
                                                                  passengersWeight: data.passengersWeight,
                                                                  cargosWeight: data.cargoWeight,
                                                                  passengers: data.passengers, cargos: data.cargo,
                                                                  zfw: result.zPoint.weight,
                                                                  tow: result.tPoint.weight,
                                                                  lw: result.lPoint.weight,
                                                                  zfi: result.zPoint.index,
                                                                  toi: result.tPoint.index,
                                                                  li: result.lPoint.index,
                                                                  zfwMac: result.zPoint.mac,
                                                                  towMac: result.tPoint.mac,
                                                                  lwMac: result.lPoint.mac,
                                                                  stabValue: result.stabValue,
                                                                  pngString: "")
    }
  }
}
