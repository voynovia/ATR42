//
//  SettingsData.swift
//  ATR42
//
//  Created by Igor Voynov on 24.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

struct SettingsData {
  var flightNo: String = ""
  var date: String = ""
  var time: String = ""
  var from: String = ""
  var to: String = ""
  
  var type: AircraftEntity?
  var tail: AircraftTail?
  var crew: AircraftCrewCode?
  var pantry: AircraftPantryCode?
  
  var passengers: [String: Cabin] = [:]
  var passengersWeight: Double = 0
  var passengersIndex: Double = 0
  var cabinLuggage: Double = 0
  
  var cargo: [String: Cargo] = [:]
  var cargoWeight: Double = 0
  var cargoIndex: Double = 0
  
  var takeOff: Double = 0.0
  var trip: Double = 0.0
  
  init() {}
}
