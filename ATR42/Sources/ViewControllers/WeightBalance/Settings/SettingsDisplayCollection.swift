//
//  SettingsDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class SettingsDisplayCollection: DisplayCollection {
  
  weak var delegate: DisplayCollectionDelegate?
  
  var sections: [TableViewSectionViewModelType] = []
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [
      TextFieldTableViewCellModel.self,
      DateFieldTableViewCellModel.self,
      SelectTableViewCellModel.self
    ]
  }
  
  var flightNoCell: TextFieldTableViewCellModel!
  var dateCell: DateFieldTableViewCellModel!
  var timeCell: DateFieldTableViewCellModel!
  var fromCell: TextFieldTableViewCellModel!
  var toCell: TextFieldTableViewCellModel!
  
  var typeCell: SelectTableViewCellModel!
  var tailCell: SelectTableViewCellModel!
  var crewCell: SelectTableViewCellModel!
  var pantryCell: SelectTableViewCellModel!
  
  var passengersCell: SelectTableViewCellModel!
  var cargoCell: SelectTableViewCellModel!
  
  var takeOffCell: TextFieldTableViewCellModel!
  var tripCell: TextFieldTableViewCellModel!
  
  var data = SettingsData()

  private var processor: SettingsProcessor!
  
  private func setPlanCells() {
    flightNoCell = TextFieldTableViewCellModel(title: "Flight No:", isValid: false, type: .default, valueChanged: { [weak self] text in
      if text.count > 1, let strongSelf = self {
        strongSelf.flightNoCell.isValid = true
        strongSelf.flightNoCell.text = text
        strongSelf.data.flightNo = text
        strongSelf.processor.process(data: strongSelf.data)
      }
    })
    dateCell = DateFieldTableViewCellModel(title: "Date:", mode: .date, valueChanged: { [weak self] date in
      if let strongSelf = self {
        strongSelf.dateCell.value = date
        strongSelf.data.date = date
        strongSelf.processor.process(data: strongSelf.data)
      }
    })
    timeCell = DateFieldTableViewCellModel(title: "Time:", mode: .time, valueChanged: { [weak self] date in
      if let strongSelf = self {
        strongSelf.timeCell.value = date
        strongSelf.data.time = date
        strongSelf.processor.process(data: strongSelf.data)
      }
    })
    fromCell = TextFieldTableViewCellModel(title: "From:", isValid: false, type: .default, valueChanged: { [weak self] text in
      if text.count > 3, let strongSelf = self {
        strongSelf.fromCell.isValid = true
        strongSelf.fromCell.text = text
        strongSelf.data.from = text
        strongSelf.processor.process(data: strongSelf.data)
      }
    })
    toCell = TextFieldTableViewCellModel(title: "To:", isValid: false, type: .default, valueChanged: { [weak self] text in
      if text.count > 3, let strongSelf = self {
        strongSelf.toCell.isValid = true
        strongSelf.toCell.text = text
        strongSelf.data.to = text
        strongSelf.processor.process(data: strongSelf.data)
      }
    })
    sections.append(TableViewSection(titleForHeader: "PLAN", titleForFooter: nil, rows: [flightNoCell, dateCell, timeCell, fromCell, toCell]))
  }
  
  private func setConfigurationCells() {
    typeCell = SelectTableViewCellModel(title: "Type:", selected: { [weak self] in
      let vc = Storyboards.Types.instantiateInitialViewController()
      vc.modalPresentationStyle = .formSheet
      if let tailsVC = vc.topViewController as? TypesViewController {
        tailsVC.selectedType = { type in
          self?.typeCell.value = type.name
          self?.data.type = type
          self?.tailCell.value = ""
          self?.data.tail = nil
          self?.crewCell.value = ""
          self?.data.crew = nil
          self?.pantryCell.value = ""
          self?.data.pantry = nil
          self?.cleanWeightCells()
          self?.delegate?.updateUI()
          
          let vc = Storyboards.Graph.instantiateInitialViewController()
          if let graphVC = vc.topViewController as? GraphViewController, let strongSelf = self {
            graphVC.wab = WABController(for: type)
            strongSelf.processor.graphController = graphVC
            strongSelf.processor.process(data: strongSelf.data)
            if UIDevice.current.userInterfaceIdiom == .pad {
              strongSelf.delegate?.showDetail(viewController: vc)
            }
          }
          
          self?.processor.clean()
        }
        self?.delegate?.present(viewController: vc)
      }
    })
    tailCell = SelectTableViewCellModel(title: "Tail:", selected: { [weak self] in
      let vc = Storyboards.Tails.instantiateInitialViewController()
      vc.modalPresentationStyle = .formSheet
      if let tailsVC = vc.topViewController as? TailsViewController, let type = self?.data.type {
        tailsVC.aircraft = type
        tailsVC.selectedTail = { tail in
          self?.tailCell.value = tail.name
          self?.data.tail = tail
          self?.crewCell.value = ""
          self?.data.crew = nil
          self?.pantryCell.value = ""
          self?.data.pantry = nil
          self?.cleanWeightCells()
          self?.delegate?.updateUI()
          self?.processor.clean()
        }
        self?.delegate?.present(viewController: vc)
      }
    })
    crewCell = SelectTableViewCellModel(title: "Crew Code:", selected: { [weak self] in
      let vc = Storyboards.Crews.instantiateInitialViewController()
      vc.modalPresentationStyle = .formSheet
      if let crewsVC = vc.topViewController as? CrewsViewController, let tail = self?.data.tail {
        crewsVC.tail = tail
        crewsVC.selectedCrew = { crew in
          self?.crewCell.value = "\(crew.cockpit) / \(crew.cabin)"
          self?.data.crew = crew
          self?.pantryCell.value = ""
          self?.data.pantry = nil
          self?.cleanWeightCells()
          self?.delegate?.updateUI()
          self?.processor.clean()
        }
        self?.delegate?.present(viewController: vc)
      }
    })
    pantryCell = SelectTableViewCellModel(title: "Pantry Code:", selected: { [weak self] in
      let vc = Storyboards.Pantry.instantiateInitialViewController()
      vc.modalPresentationStyle = .formSheet
      if let pantryVC = vc.topViewController as? PantryViewController,
        let tail = self?.data.tail,
        let crew = self?.data.crew {
        pantryVC.tail = tail
        pantryVC.crew = crew
        pantryVC.selectedPantry = { pantry in
          guard let strongSelf = self else { return }
          strongSelf.pantryCell.value = pantry.name
          strongSelf.data.pantry = pantry
          strongSelf.cleanWeightCells()
          strongSelf.delegate?.updateUI()
          strongSelf.processor.process(data: strongSelf.data)
        }
        self?.delegate?.present(viewController: vc)
      }
    })
    sections.append(TableViewSection(titleForHeader: "CONFIGURATION", titleForFooter: nil, rows: [typeCell, tailCell, crewCell, pantryCell]))
  }
  
  private func setWeightCells() {
    passengersCell = SelectTableViewCellModel(title: "Passengers:", selected: { [weak self] in
      guard self?.data.tail != nil, self?.data.crew != nil, self?.data.pantry != nil else { return }
      let vc = Storyboards.Weights.instantiateInitialViewController()
      if let passengersVC = vc.topViewController as? WeightsViewController, let strongSelf = self {
        passengersVC.aircraft = strongSelf.data.type
        passengersVC.tail = strongSelf.data.tail
        passengersVC.selectableType = .passengers
        passengersVC.secondAttendant = self?.data.crew?.cabin == 2 ? true : false
        passengersVC.passengers = strongSelf.data.passengers
        passengersVC.cabinLuggage = strongSelf.data.cabinLuggage
        passengersVC.selectedPassengers = { value in
          self?.passengersCell.value = String(value.weight)
          self?.data.passengersWeight = value.weight
          self?.data.passengersIndex = value.index
          self?.data.passengers = value.passengers
          self?.data.cabinLuggage = value.luggage
          self?.delegate?.updateUI()
          self?.processor.process(data: strongSelf.data)
        }
        vc.modalPresentationStyle = .formSheet
        vc.preferredContentSize = CGSize(width: UIScreen.main.bounds.width/2, height: 200)
        self?.delegate?.present(viewController: vc)
      }
    })
    cargoCell = SelectTableViewCellModel(title: "Cargo:", selected: { [weak self] in
      guard self?.data.tail != nil, self?.data.crew != nil, self?.data.pantry != nil else { return }
      let vc = Storyboards.Weights.instantiateInitialViewController()
      if let cargoVC = vc.topViewController as? WeightsViewController, let strongSelf = self {
        cargoVC.aircraft = strongSelf.data.type
        cargoVC.tail = strongSelf.data.tail
        cargoVC.selectableType = .cargos
        cargoVC.cargos = strongSelf.data.cargo
        cargoVC.selectedCargo = { value in
          self?.cargoCell.value = String(value.weight)
          self?.data.cargoWeight = value.weight
          self?.data.cargoIndex = value.index
          self?.data.cargo = value.cargo
          self?.delegate?.updateUI()
          self?.processor.process(data: strongSelf.data)
        }
        vc.modalPresentationStyle = .formSheet
        vc.preferredContentSize = CGSize(width: UIScreen.main.bounds.width/2, height: 200)
        self?.delegate?.present(viewController: vc)
      }
    })
    sections.append(TableViewSection(titleForHeader: "WEIGHTS", titleForFooter: nil, rows: [passengersCell, cargoCell]))
  }
  
  private func setFuelCells() {
    takeOffCell = TextFieldTableViewCellModel(title: "Take Off:", isValid: false, type: .decimalPad, valueChanged: { [weak self] text in
      if let digit = Double(text.replacingOccurrences(of: ",", with: ".")), let strongSelf = self {
        self?.takeOffCell.isValid = true
        self?.takeOffCell.text = text
        self?.data.takeOff = digit
        self?.processor.process(data: strongSelf.data)
      }
    })
    tripCell = TextFieldTableViewCellModel(title: "Trip:", isValid: false, type: .decimalPad, valueChanged: { [weak self] text in
      if let digit = Double(text.replacingOccurrences(of: ",", with: ".")), let strongSelf = self {
        self?.tripCell.isValid = true
        self?.tripCell.text = text
        self?.data.trip = digit
        self?.processor.process(data: strongSelf.data)
      }
    })
    sections.append(TableViewSection(titleForHeader: "FUEL", titleForFooter: nil, rows: [takeOffCell, tripCell]))
  }
  
  init(delegate: DisplayCollectionDelegate, processor: SettingsProcessor) {
    self.delegate = delegate
    self.processor = processor
    
    setPlanCells()
    setConfigurationCells()
    setWeightCells()
    setFuelCells()
  }
  
  private func cleanWeightCells() {
    passengersCell.value = ""
    data.passengersWeight = 0
    data.passengersIndex = 0
    data.passengers = [:]
    data.cabinLuggage = 0
    cargoCell.value = ""
    data.cargoWeight = 0
    data.cargoIndex = 0
    data.cargo = [:]
  }
  
  public func clean() {
    for section in sections {
      for row in section.rows {
        switch row {
        case (let textFieldRow as TextFieldTableViewCellModel):
          textFieldRow.text = ""
        case (let dateFieldRow as DateFieldTableViewCellModel):
          dateFieldRow.value = ""
        case (let selectFieldRow as SelectTableViewCellModel):
          selectFieldRow.value = ""
        default:
          break
        }
      }
    }
    data = SettingsData()
    delegate?.updateUI()
    processor.clean()
  }
  
  var numberOfSections: Int {
    return sections.count
  }
  
  func numberOfRows(in section: Int) -> Int {
    return sections[section].rows.count
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    return sections[indexPath.section].rows[indexPath.row]
  }
  
  func titleForHeader(in section: Int) -> String? {
    return sections[section].titleForHeader
  }
}
