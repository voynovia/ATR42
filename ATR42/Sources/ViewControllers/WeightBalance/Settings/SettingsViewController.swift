//
//  SettingsViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
  
  @IBOutlet weak var panelHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var tableView: UITableView! {
    didSet {
      var configuration = TableViewConfiguration.default
      configuration.topInset = 8
      configuration.bottomInset = 8
      configuration.estimatedRowHeight = 60
      tableView.configure(with: .custom(configuration))
    }
  }
  @IBAction func graphButtonPressed(_ sender: UIBarButtonItem) {
    guard let vc = settingsProcessor.graphController else { return }
    showDetail(viewController: vc)
  }
  
  fileprivate var displayCollection: SettingsDisplayCollection!
  fileprivate let settingsProcessor: SettingsProcessor = SettingsProcessor()
  
  override func viewDidLoad() {
    super.viewDidLoad()

    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Clean", style: .plain, target: self, action: #selector(clean(_:)))
    if UIDevice.current.userInterfaceIdiom == .pad {
      navigationItem.rightBarButtonItems?.removeAll()
    }
          
    displayCollection = SettingsDisplayCollection(delegate: self, processor: settingsProcessor)
    tableView.registerNibs(from: displayCollection)
    tableView.tableFooterView = UIView()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.allowsSelectionDuringEditing = true
    tableView.allowsSelection = true
    
    navigationItem.title = "Settings"
    if #available(iOS 11.0, *) {
      navigationItem.largeTitleDisplayMode = .never
    }
    
    panelHeightConstraint.constant = 44 * 3
    keyboardDelegate = self
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    view.backgroundColor = tableView.backgroundColor
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue == SettingsViewController.Segue.panel, let vc = segue.destination as? PanelViewController {
      settingsProcessor.panelController = vc
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadDataImmediately()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
      self.settingsProcessor.process(data: self.displayCollection.data)
    }
  }
  
  @objc private func clean(_ sender: UIBarButtonItem) {
    displayCollection.clean()
  }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return displayCollection.numberOfSections
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return displayCollection.numberOfRows(in: section)
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = displayCollection.model(for: indexPath)
    let cell = tableView.dequeueReusableCell(for: indexPath, with: model)
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return displayCollection.titleForHeader(in: section)
  }
}

extension SettingsViewController: KeyboardHandlerDelegate {
  func keyboardStateChanged(input: UIView?, state: KeyboardState, info: KeyboardInfo) {
    var scrollViewContentInsets = tableView.contentInset
    var indicatorInsets = tableView.scrollIndicatorInsets
    switch state {
    case .frameChanged, .opened:
      if UIDevice.current.userInterfaceIdiom == .pad && UIDevice.current.orientation.isPortrait {
        scrollViewContentInsets.bottom = tableView.defaultBottomInset + 44
      } else {
        scrollViewContentInsets.bottom = info.endFrame.height + tableView.defaultBottomInset + 44
        indicatorInsets.bottom = info.endFrame.height
      }
    case .hidden:
      scrollViewContentInsets.bottom = 0
      indicatorInsets.bottom = 0
    }
    tableView.contentInset = scrollViewContentInsets
    tableView.scrollIndicatorInsets = indicatorInsets
    info.animate ({ [weak self] in
      self?.view.layoutIfNeeded()
    })
  }
}
