//
//  GraphViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 26.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController {

  public var wab: WABController?
  
  public var tPoint: Point?, lPoint: Point?, zPoint: Point?
  public var stabValue: Double = 0
  public var printValues: PrintValues?
  
  private var graphView: UIView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(share(_:)))
    graphView = UIView()
    view.addSubview(graphView)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    graphView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
    wab?.graph.draw(at: graphView)
    update()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    guard let graphView = graphView else { return }
    graphView.frame.size = CGSize(width: size.width, height: size.height)
    self.graphView.subviews.forEach { subview in
      subview.removeFromSuperview()
    }
    wab?.graph.draw(at: graphView)
  }
  
  public func clean() {
    wab?.graph.clean()
  }
  
  public func update() {
    guard let tPoint = tPoint, let lPoint = lPoint, let zPoint = zPoint else { return }
    wab?.graph.update(tPoint: tPoint, lPoint: lPoint, zPoint: zPoint, stabValue: stabValue)
  }
  
  @objc private func share(_ sender: UIBarButtonItem) {
    guard var printValues = printValues,
      let pngString = UIImagePNGRepresentation(UIImage(view: graphView))?.base64EncodedString() else { return }
    printValues.pngString = pngString
    
    let pdfController = PDFController(values: printValues)
    pdfController.getPDF()
    
    if UIPrintInteractionController.canPrint(pdfController.url) {
      let printInfo = UIPrintInfo(dictionary: nil)
      printInfo.jobName = pdfController.url.lastPathComponent
      printInfo.outputType = .grayscale
      let printInteractionController = UIPrintInteractionController.shared
      printInteractionController.printInfo = printInfo
      printInteractionController.showsPaperSelectionForLoadedPapers = true
      printInteractionController.showsNumberOfCopies = true
      printInteractionController.printFormatter = pdfController.printFormatter
      printInteractionController.present(animated: true, completionHandler: nil)
    }
    
  }
}
