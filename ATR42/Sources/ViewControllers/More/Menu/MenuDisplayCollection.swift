//
//  MenuDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

class MenuDisplayCollection: DisplayCollection {
  
  weak var delegate: DisplayCollectionDelegate?
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [MenuTableViewCellModel.self]
  }
  
  var numberOfSections: Int {
    return 1
  }
  
  func numberOfRows(in section: Int) -> Int {
    return 2
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    switch indexPath.row {
    case 0:
      return MenuTableViewCellModel(title: "Feedback")
    default:
      return MenuTableViewCellModel(title: "About")
    }
  }
  
  func select(_ indexPath: IndexPath) {
    switch indexPath.row {
    case 0:
      let vc = Storyboards.Feedback.instantiateInitialViewController()
      delegate?.showDetail(viewController: vc)
    default:
      let vc = Storyboards.About.instantiateInitialViewController()
      delegate?.showDetail(viewController: vc)
    }
  }
}
