//
//  MenuViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  
  fileprivate var displayCollection = MenuDisplayCollection()
  
  @IBOutlet weak var tableView: UITableView!  {
    didSet {
      var configuration = TableViewConfiguration.default
      configuration.topInset = 8
      configuration.bottomInset = 8
      configuration.estimatedRowHeight = 100
      tableView.configure(with: .custom(configuration))
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    title = "Menu"
    displayCollection.delegate = self
    
    tableView.dataSource = self
    tableView.delegate = self
    tableView.registerNibs(from: displayCollection)
    tableView.separatorColor = .darkGray
    tableView.tableFooterView = UIView()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.tableView.reloadDataImmediately()
  }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return displayCollection.numberOfSections
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return displayCollection.numberOfRows(in: section)
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = displayCollection.model(for: indexPath)
    let cell = tableView.dequeueReusableCell(for: indexPath, with: model)
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    displayCollection.select(indexPath)
    tableView.deselectRow(at: indexPath, animated: false)
  }
}
