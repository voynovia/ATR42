//
//  AboutViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
  
  @IBOutlet weak var versionLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = "About"
    view.backgroundColor = UIColor(.backgroundColor)
    buildInfo()
  }
  
  private func buildInfo() {
    guard let bundleVer = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
      let bundleBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else {
        return
    }
    versionLabel.text = "\(bundleVer).\(bundleBuild)"
    
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "dd.MM.yy"
    dateLabel.text = dateFormatter.string(from: compileDate)
  }
  
  private var compileDate: Date {
    let bundleName = Bundle.main.infoDictionary!["CFBundleName"] as? String ?? "Info.plist"
    if let infoPath = Bundle.main.path(forResource: bundleName, ofType: nil),
      let infoAttr = try? FileManager.default.attributesOfItem(atPath: infoPath),
      let infoDate = infoAttr[FileAttributeKey.creationDate] as? Date {
      return infoDate
    }
    return Date()
  }
}
