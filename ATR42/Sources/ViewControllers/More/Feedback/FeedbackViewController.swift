//
//  FeedbackViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

import UIKit

class FeedbackViewController: UIViewController {
  
  fileprivate var displayCollection: FeedbackDisplayCollection!

  @IBOutlet weak var dummyView: UIView!
  @IBOutlet weak var textLabel: UILabel!
  
  @IBOutlet weak var tableView: UITableView! {
    didSet {
      var configuration = TableViewConfiguration.default
      configuration.topInset = 8
      configuration.bottomInset = 8
      configuration.estimatedRowHeight = 60
      tableView.configure(with: .custom(configuration))
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Feedback"
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(send))
    
    displayCollection = FeedbackDisplayCollection(delegate: self)
    tableView.registerNibs(from: displayCollection)
    tableView.tableFooterView = UIView()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.allowsSelectionDuringEditing = true
    
    dummyView.backgroundColor = UIColor(hexString: "f7f7f9")
    textLabel.textColor = .lightGray
    
    keyboardDelegate = self
  }
  
  @objc private func send() {
    if let failedFieldIndexPath = displayCollection.failedField {
      tableView.failedShakeRow(failedFieldIndexPath)
      return
    }
    displayCollection.sendContactInfo { text in
      DispatchQueue.main.async {
        self.textLabel.text = text
        self.view.bringSubview(toFront: self.dummyView)
        self.navigationItem.rightBarButtonItem = nil
      }
    }
  }
}

extension FeedbackViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return displayCollection.numberOfSections
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return displayCollection.numberOfRows(in: section)
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = displayCollection.model(for: indexPath)
    let cell = tableView.dequeueReusableCell(for: indexPath, with: model)
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return displayCollection.titleForHeader(in: section)
  }
  func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    return nil
  }
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 16
  }
}

extension FeedbackViewController: KeyboardHandlerDelegate {
  func keyboardStateChanged(input: UIView?, state: KeyboardState, info: KeyboardInfo) {
    var scrollViewContentInsets = tableView.contentInset
    var indicatorInsets = tableView.scrollIndicatorInsets
    switch state {
    case .frameChanged, .opened:
      let scrollViewBottomInset = info.endFrame.height + tableView.defaultBottomInset
      scrollViewContentInsets.bottom = scrollViewBottomInset
      indicatorInsets.bottom = info.endFrame.height
    case .hidden:
      scrollViewContentInsets.bottom = 0
      indicatorInsets.bottom = 0
    }
    tableView.contentInset = scrollViewContentInsets
    tableView.scrollIndicatorInsets = indicatorInsets
    info.animate ({ [weak self] in
      self?.view.layoutIfNeeded()
    })
  }
}
