//
//  FeedbackDisplayCollection.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

class FeedbackDisplayCollection: DisplayCollection {
  
  weak var delegate: DisplayCollectionDelegate?

  var sections: [TableViewSectionViewModelType] = []
  
  static var modelsForRegistration: [CellViewAnyModelType.Type] {
    return [
      TextFieldBigTableViewCellModel.self,
      TextViewTableViewCellModel.self
    ]
  }
  
  var numberOfSections: Int {
    return sections.count
  }
  
  var failedField: IndexPath? {
    for (sectionId, section) in sections.enumerated() {
      for (rowId, row) in section.rows.enumerated() {
        if row.isValid == false {
          return IndexPath(row: rowId, section: sectionId)
        }
      }
    }
    return nil
  }
  
  var nameCell: TextFieldBigTableViewCellModel!
  var mailCell: TextFieldBigTableViewCellModel!
  var messageCell: TextViewTableViewCellModel!
  
  var name: String = ""
  var mail: String = ""
  var message: String = ""
  
  init(delegate: DisplayCollectionDelegate?) {
    self.delegate = delegate
    
    nameCell = TextFieldBigTableViewCellModel(isValid: false, type: .default) { [weak self] text in
      if text.count > 1 {
        self?.nameCell.isValid = true
        self?.name = text
      }
    }
    
    mailCell = TextFieldBigTableViewCellModel(isValid: false, type: .emailAddress) { [weak self] text in
      if text.count > 5 {
        self?.mailCell.isValid = true
        self?.mail = text
      }
    }
    
    messageCell = TextViewTableViewCellModel(text: "", isValid: false, valueChanged: { [weak self] text in
      if text.count > 3 {
        self?.messageCell.isValid = true
        self?.message = text
      }
    })

    sections.append(TableViewSection(titleForHeader: "FULLNAME", titleForFooter: nil, rows: [nameCell]))
    sections.append(TableViewSection(titleForHeader: "E-MAIL", titleForFooter: nil, rows: [mailCell]))
    sections.append(TableViewSection(titleForHeader: "MESSAGE", titleForFooter: nil, rows: [messageCell]))
  }
  
  func numberOfRows(in section: Int) -> Int {
    return sections[section].rows.count
  }
  
  func model(for indexPath: IndexPath) -> CellViewAnyModelType {
    nameCell.text = name
    mailCell.text = mail
    messageCell.text = message
    return sections[indexPath.section].rows[indexPath.row]
  }
  
  func titleForHeader(in section: Int) -> String? {
    return sections[section].titleForHeader
  }
  
  // MARK: - save account
  
  func sendContactInfo(completion: @escaping (String) -> Void) {
    var application: String = "Unknown"
    if let bundleVer = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
      let bundleBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String,
      let name = Bundle.main.infoDictionary?["CFBundleName"] as? String {
        application = name + " " + bundleVer + "." + bundleBuild
    }

    let text = """
    Приложение: \(application)
    ФИО: \(name)
    email: \(mail)
    сообщение: \(message)
    """
    TelegramController.sendMessage(text) { error in
      if let error = error {
        completion(error.localizedDescription)
      } else {
        completion("""
          Thanks, message in processing.
          """.localized)
      }
    }
  }
}
