//
//  MoreSplitViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class MoreSplitViewController: UISplitViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    preferredDisplayMode = .allVisible
    delegate = self
  }
  
  override func az_tabBarItemContentView() -> AZTabBarItemView {
    return TabBarItemView.create(with: .more)
  }
  
}

extension MoreSplitViewController: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {
    guard let secondaryAsNav = secondaryViewController as? UINavigationController,
      (secondaryAsNav.topViewController as? MenuViewController) != nil else {
        return true
    }
    return false
  }
}
