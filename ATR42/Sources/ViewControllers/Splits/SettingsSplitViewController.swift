//
//  SettingsSplitViewController.swift
//  ATR42
//
//  Created by Igor Voynov on 05.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

class SettingsSplitViewController: UISplitViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    delegate = self
    preferredDisplayMode = .automatic
  }
  
  override func az_tabBarItemContentView() -> AZTabBarItemView {
    return TabBarItemView.create(with: .weightBalance)
  }
  
}

extension SettingsSplitViewController: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {
    guard let secondaryAsNav = secondaryViewController as? UINavigationController,
      (secondaryAsNav.topViewController as? SettingsViewController) != nil else {
        return true
    }
    return false
  }
}
