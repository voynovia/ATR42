//
//  MethodSwizzler.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

struct SwizzlerController {
  static func swizzleMethods(objectClass: AnyClass, originalSelector: Selector, swizzledSelector: Selector) {
    guard let originalMethod = class_getInstanceMethod(objectClass, originalSelector),
      let swizzledMethod = class_getInstanceMethod(objectClass, swizzledSelector) else {
        return
    }
    let methodAdded = class_addMethod(objectClass,
                                      originalSelector,
                                      method_getImplementation(swizzledMethod),
                                      method_getTypeEncoding(swizzledMethod))
    
    if methodAdded {
      class_replaceMethod(objectClass,
                          swizzledSelector,
                          method_getImplementation(originalMethod),
                          method_getTypeEncoding(originalMethod))
    } else {
      method_exchangeImplementations(originalMethod, swizzledMethod)
    }
  }
}
