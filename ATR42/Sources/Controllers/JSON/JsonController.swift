//
//  JsonController.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Marshal

class JsonController {
  
  static func jsonObjectFromData(_ data: Data) throws -> [String: Any] {
    return try JSONParser.JSONObjectWithData(data)
  }
  
  static func arrayObjectsFromData(_ data: Data) throws -> [[String: Any]] {
    return try JSONParser.JSONArrayWithData(data)
  }
  
  static func objectFromData<T: Unmarshaling>(_ data: Data) throws -> T {
    let json = try JSONParser.JSONObjectWithData(data)
    return try T(object: json)
  }
  
  static func objectsFromData<T: Unmarshaling>(_ data: Data) throws -> [T] {
    return try JSONParser.JSONArrayWithData(data).map(T.init)
  }
  
  static func objectFromJson<T: Unmarshaling>(_ json: [String: Any], by key: String) throws -> T {
    return try json.value(for: key)
  }
  
  static func objectsFromJson<T: Unmarshaling>(_ json: [String: Any], by key: String) throws -> [T] {
    return try json.value(for: key)
  }
  
  static func dataFromJson<T: Marshaling>(_ object: T) throws -> Data? {
    return try (object.marshaled() as? [String: Any])?.jsonData()
  }
  
  static func dataFromJson<T: Marshaling>(_ objects: [T]) throws -> Data {
    let json = try objects.map { object -> [String: Any] in
      guard let json = object.marshaled() as? [String: Any] else {
        throw NSError("marshaled error", code: 5011)
      }
      return json
    }
    return try json.jsonData()
  }
  
  static func stringFromJson(_ json: [String: Any]) throws -> String? {
    guard let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
      let string = String(data: jsonData, encoding: .utf8) else {
        return nil
    }
    return string
  }
}
