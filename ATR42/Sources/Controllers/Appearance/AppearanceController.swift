//
//  AppearanceController.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

struct AppearanceController {
  static func setupAppearance() {
    setupStatusBar()
    setupNavigationBar()
    setupTableView()
    setupTextField()
    setupTextView()
  }
  
  private static func setupStatusBar() {
    UIApplication.shared.statusBarStyle = .lightContent
  }
  
  private static func setupNavigationBar() {
    UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0.2156862745, green: 0.2156862745, blue: 0.2235294118, alpha: 1)
    UINavigationBar.appearance().isTranslucent = false
    UINavigationBar.appearance().tintColor = .white
    UINavigationBar.appearance().barStyle = .black
    
    if #available(iOS 11.0, *) {
      UINavigationBar.appearance().prefersLargeTitles = true
    }
  }
  
  private static func setupTableView() {
    UITableView.appearance().separatorColor = .clear
    UITableView.appearance().backgroundColor = #colorLiteral(red: 0.2274509804, green: 0.2274509804, blue: 0.2352941176, alpha: 1)
    UITableViewCell.appearance().backgroundColor = UIColor.white.withAlphaComponent(0.02)
    
    let selectedView = UIView()
    selectedView.backgroundColor = .clear
    UITableViewCell.appearance().selectedBackgroundView = selectedView
  }
  
  private static func setupTextField() {
    UITextField.appearance().font = UIFont.systemFont(ofSize: 18)
    UITextField.appearance().keyboardAppearance = .dark
    UITextField.appearance().backgroundColor = .darkGray
    UITextField.appearance().textColor = .white
    UITextField.appearance().borderStyle = .roundedRect
  }
  
  private static func setupTextView() {
    UITextView.appearance().backgroundColor = .darkGray
    UITextView.appearance().tintColor = .white
  }

}
