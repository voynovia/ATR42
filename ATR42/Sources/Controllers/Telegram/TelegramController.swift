//
//  TelegramController.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

struct TelegramController {
  
  static let chatID = "1220543"
  
  static func sendMessage(_ text: String, completion: @escaping (NetworkError?) -> Void) {
    guard let urlText = text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
      let url = URL(string: "https://api.telegram.org/bot491977322:AAFwj2fJZ6D0z30h1WPQUEzWACxnZ8znQCE/sendmessage?chat_id=\(chatID)&text=\(urlText)") else {
        completion(.encoding)
        return
    }
    let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
      if let data = data {
        if let jsonSerialized = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
          let json = jsonSerialized, let ok = json["ok"] as? Bool {
          completion(ok ? nil : .server)
        } else {
          completion(.server)
        }
      } else {
        completion(.network)
      }
    }
    task.resume()
  }
}
