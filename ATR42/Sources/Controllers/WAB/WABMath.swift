//
//  Math.swift
//  WaB
//
//  Created by Igor Voynov on 19.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

struct WABMath {
  
  private static let kConstant: Double = 0
  private static let cConstant: Double = 100
  
  public static func index(for aircraft: AircraftEntity, sta: Double) -> Double {
    return (sta - aircraft.refSta) / 100
  }

  public static func index(for aircraft: AircraftEntity, prMac: Double, weight: Double) -> Double {
    return (((prMac * aircraft.mac/100) - aircraft.refSta + aircraft.lemac) * weight / cConstant) + kConstant
  }

  public static func prMAC(for aircraft: AircraftEntity, index: Double, weight: Double) -> Double {
    return (((cConstant*(index-kConstant))/weight) + aircraft.refSta - aircraft.lemac) / (aircraft.mac / 100)
  }

  public static func stab(for aircraft: AircraftEntity, mac: Double) -> Double {
    let pr = (mac - Double(aircraft.stabMin.mac)) * 100
      / Double(aircraft.stabMax.mac - aircraft.stabMin.mac)
    return Double(aircraft.stabMax.weight - aircraft.stabMin.weight) * pr
      / 100 + aircraft.stabMin.weight
  }

  private static func isPointInside(points: [AircraftWeightIndex], weight: Double, index: Double) -> Bool {
    var inside = false
    var j = points.count - 1
    for i in 0..<points.count {
      let xi = points[i].index, yi = points[i].weight
      let xj = points[j].index, yj = points[j].weight
      if ((yi > weight) != (yj > weight))
        && (index < (xj - xi) * (weight - yi) / (yj - yi) + xi) {
        inside = !inside
      }
      j = i
    }
    return inside
  }

  public static func isGood(box: AircraftBox, weight: Double, index: Double) -> Bool {
    let forwardPoints = box.forward.sorted(by: {$0.weight < $1.weight})
    let aftPoints = box.aft.sorted(by: {$0.weight > $1.weight})
    let points = forwardPoints + aftPoints
    return isPointInside(points: points, weight: weight, index: index)
  }
}
