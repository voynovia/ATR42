//
//  GraphController.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public typealias Result = (tPoint: Point, lPoint: Point, zPoint: Point, stabValue: Double)

internal var aircraft: AircraftEntity!

class WABController {

  lazy var graph: GraphController = GraphController()
  
  public init(for air: AircraftEntity) {
    aircraft = air
    graph.clean()
  }
  
  public func getFuelIndex(block: Double) -> Double {
    var diff: Double = Double(Int.max)
    var fuelEffect: AircraftWeightIndex!
    for effect in aircraft.fuelEffects {
      let diffEffect = abs(block - effect.weight)
      if diffEffect < diff {
        diff = diffEffect
        fuelEffect = effect
      } else {
        break
      }
    }
    
    let fuelEffectPrMac = WABMath.prMAC(for: aircraft, index: fuelEffect.index, weight: fuelEffect.weight)
    return WABMath.index(for: aircraft, prMac: fuelEffectPrMac, weight: block)
  }
  
  // swiftlint:disable:next function_parameter_count
  public func process(doi: Double, dow: Double,
                      passengersIndex: Double, passengersWeight: Double,
                      cargosIndex: Double, cargosWeight: Double,
                      fuelBlock: Double, fuelTrip: Double,
                      completion: (Result) -> Void) {
    let zfw = dow + cargosWeight + passengersWeight
    let zfi = doi + passengersIndex + cargosIndex
    let tow = zfw + fuelBlock // - aircraft.fuelTaxi
    let toi = zfi + getFuelIndex(block: fuelBlock)
    let lw = tow - fuelTrip
    let li = toi - getFuelIndex(block: fuelTrip)
    
    guard let mtowBox = aircraft.boxes.first(where: {$0.type == "mtow"}),
      let mlwBox = aircraft.boxes.first(where: {$0.type == "mlw"}),
      let mzfwBox = aircraft.boxes.first(where: {$0.type == "mzfw"}) else { return }
    
    let tPoint = Point(index: toi,
                       weight: tow,
                       mac: WABMath.prMAC(for: aircraft, index: toi, weight: tow),
                       isGood: WABMath.isGood(box: mtowBox, weight: tow, index: toi))
    
    let lPoint = Point(index: li,
                       weight: lw,
                       mac: WABMath.prMAC(for: aircraft, index: li, weight: lw),
                       isGood: WABMath.isGood(box: mlwBox, weight: lw, index: li))
    
    let zPoint = Point(index: zfi,
                       weight: zfw,
                       mac: WABMath.prMAC(for: aircraft, index: zfi, weight: zfw),
                       isGood: WABMath.isGood(box: mzfwBox, weight: zfw, index: zfi))
    
    let stabMac = WABMath.prMAC(for: aircraft, index: toi, weight: tow)
    let stabValue = WABMath.stab(for: aircraft, mac: stabMac)
        
    completion((tPoint: tPoint, lPoint: lPoint, zPoint: zPoint, stabValue: stabValue))
  }
}
