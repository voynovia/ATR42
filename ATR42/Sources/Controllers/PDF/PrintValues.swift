//
//  PrintValues.swift
//  WaB
//
//  Created by Igor Voynov on 25.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public struct PrintValues {
  
  public let flight: String!
  public let date: String!
  public let time: String!
  public let from: String!
  public let to: String!
  
  public let tail: String!
  public let crew: String!
  public let pantry: String!
  public let baseIndex: Double!
  public let zfwMax: Double!
  public let towMax: Double!
  public let lwMax: Double!
  
  public let version: String!
  
  public let dow: Double!
  public let doi: Double!
  public let takeOffFuel: Double!
  public let tripFuel: Double!
  public let passengersWeight: Double!
  public let cargosWeight: Double!
  
  public let passengers: [String: Cabin]
  public let cargos: [String: Cargo]
  
  public let zfw: Double!
  public let tow: Double!
  public let lw: Double!
  public let zfi: Double!
  public let toi: Double!
  public let li: Double!
  public let zfwMac: Double!
  public let towMac: Double!
  public let lwMac: Double!
  public let stabValue: Double!
  
  public var pngString: String!
  
  public init(flight: String, date: String, time: String, from: String, to: String,
              tail: String, crew: String, pantry: String, baseIndex: Double,
              zfwMax: Double, towMax: Double, lwMax: Double, version: String,
              dow: Double, doi: Double, takeOffFuel: Double, tripFuel: Double, passengersWeight: Double, cargosWeight: Double,
              passengers: [String: Cabin], cargos: [String: Cargo],
              zfw: Double, tow: Double, lw: Double, zfi: Double, toi: Double, li: Double,
              zfwMac: Double, towMac: Double, lwMac: Double, stabValue: Double, pngString: String) {
    self.flight = flight
    self.date = date
    self.time = time
    self.from = from
    self.to = to
    self.tail = tail
    self.crew = crew
    self.pantry = pantry
    self.baseIndex = baseIndex
    self.zfwMax = zfwMax
    self.towMax = towMax
    self.lwMax = lwMax
    self.version = version
    self.dow = dow
    self.doi = doi
    self.takeOffFuel = takeOffFuel
    self.tripFuel = tripFuel
    self.passengersWeight = passengersWeight
    self.cargosWeight = cargosWeight
    self.passengers = passengers
    self.cargos = cargos
    self.zfw = zfw
    self.tow = tow
    self.lw = lw
    self.zfi = zfi
    self.toi = toi
    self.li = li
    self.zfwMac = zfwMac
    self.towMac = towMac
    self.lwMac = lwMac
    self.stabValue = stabValue
    self.pngString = pngString
  }
}
