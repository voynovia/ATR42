//
//  ShareController.swift
//  ATR42
//
//  Created by Igor Voynov on 21.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation
import WebKit

public class PDFController {
  
  private var fileName: String!
  private var values: PrintValues!
  
  public var url: URL {
    guard let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
      fatalError("Error getting user's document directory.")
    }
    return directory.appendingPathComponent(fileName).appendingPathExtension("pdf")
  }
  
  public init(values: PrintValues) {
    self.values = values
    self.fileName = "wab-\(Int(Date().timeIntervalSince1970))"
  }
  
  private func getHTML(from fileName: String) -> String {
    guard let htmlFile = Bundle.main.url(forResource: fileName, withExtension: nil) else {
      fatalError("Error locating HTML file.")
    }
    guard var htmlContent = try? String(contentsOf: htmlFile) else {
      fatalError("Error getting HTML file content.")
    }

    let stab = values.stabValue.rounded(toPlaces: 2)
    
    var cargos = ""
    for cargo in values.cargos.sorted(by: {$0.key > $1.key}) {
      cargos += "<td>\(cargo.key) /</td>"
      cargos += "<td class='left'>\(Int(cargo.value.weight))</td>"
    }
    
    var kinds: [String: Int] = [:]
    for passenger in values.passengers {
      passenger.value.passengers.forEach { pass in
        if kinds[pass.name] != nil {
          kinds[pass.name]! += pass.count
        } else {
          kinds[pass.name] = pass.count
        }
      }
    }
    var passengers = ""
    for kind in kinds.sorted(by: {$0.key < $1.key}) {
      passengers += "<td>\(kind.key) /</td>"
      passengers += "<td class='left'>\(kind.value)</td>"
    }
    let ttl = kinds.values.reduce(0, +)
    
    var cabins = ""
    var pax: Int = 0
    for passenger in values.passengers.sorted(by: {$0.key < $1.key}) {
      cabins += "<td>\(passenger.key) /</td>"
      let count = passenger.value.passengers.filter { $0.name != "INFNT" && $0.name != "INFANT" }.map {$0.count}.reduce(0, +)
      pax += count
      cabins += "<td class='left'>\(count)</td>"
    }
    
    // equalization
    var cargosCount = values.cargos.count
    var kindsCount = kinds.count
    var passCount = values.passengers.count
    if let maxCount = [cargosCount, kindsCount, passCount].max() {
      while cargosCount < maxCount {
        cargos += "<td>/</td><td></td>"
        cargosCount += 1
      }
      while kindsCount < maxCount {
        passengers += "<td>/</td><td></td>"
        kindsCount += 1
      }
      while passCount < maxCount {
        cabins += "<td>/</td><td></td>"
        passCount += 1
      }
    }
    
    if let bundleVer = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
      let bundleBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
      htmlContent = htmlContent.replacingOccurrences(of: "{{version}}", with: "ATR V\(bundleVer).\(bundleBuild)")
    }
    
    return htmlContent
      .replacingOccurrences(of: "{{from}}", with: values.from)
      .replacingOccurrences(of: "{{to}}", with: values.to)
      .replacingOccurrences(of: "{{flight}}", with: values.flight)
      .replacingOccurrences(of: "{{mod}}", with: values.tail)
      .replacingOccurrences(of: "{{crew}}", with: values.crew)
      .replacingOccurrences(of: "{{pantry}}", with: values.pantry)
      .replacingOccurrences(of: "{{DATE}}", with: values.date)
      .replacingOccurrences(of: "{{TIME}}", with: values.time)
      .replacingOccurrences(of: "{{tailVersion}}", with: values.version)
      .replacingOccurrences(of: "{{takeOffFuel}}", with: "\(Int(values.takeOffFuel))")
      .replacingOccurrences(of: "{{tripFuel}}", with: "\(Int(values.tripFuel))")
      .replacingOccurrences(of: "{{DOW}}", with: "\(Int(values.dow))")
      .replacingOccurrences(of: "{{passWeight}}", with: "\(Int(values.passengersWeight))")
      .replacingOccurrences(of: "{{cargosWeight}}", with: "\(Int(values.cargosWeight))")
      .replacingOccurrences(of: "{{cargos}}", with: cargos)
      .replacingOccurrences(of: "{{passengers}}", with: passengers)
      .replacingOccurrences(of: "{{ttl}}", with: "\(ttl)")
      .replacingOccurrences(of: "{{cabins}}", with: cabins)
      .replacingOccurrences(of: "{{pax}}", with: "\(pax)")
      .replacingOccurrences(of: "{{totalWeight}}", with: "\(Int(values.passengersWeight + values.cargosWeight))")
      .replacingOccurrences(of: "{{zfw}}", with: "\(Int(values.zfw))")
      .replacingOccurrences(of: "{{zfwMax}}", with: "\(Int(values.zfwMax))")
      .replacingOccurrences(of: "{{tow}}", with: "\(Int(values.tow))")
      .replacingOccurrences(of: "{{towMax}}", with: "\(Int(values.towMax))")
      .replacingOccurrences(of: "{{lw}}", with: "\(Int(values.lw))")
      .replacingOccurrences(of: "{{lwMax}}", with: "\(Int(values.lwMax))")
      .replacingOccurrences(of: "{{BI}}", with: "\(values.baseIndex.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{zfi}}", with: "\(values.zfi.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{toi}}", with: "\(values.toi.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{li}}", with: "\(values.li.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{DOI}}", with: "\(values.doi.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{zfwMac}}", with: "\(values.zfwMac.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{towMac}}", with: "\(values.towMac.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{lwMac}}", with: "\(values.lwMac.rounded(toPlaces: 2))")
      .replacingOccurrences(of: "{{stab}}", with: stab > 0 ? "+\(stab)" : "\(stab)")
      .replacingOccurrences(of: "{{BASE64_STRING}}", with: values.pngString)
  }
  
  private func createPDF(using printFormatter: UIPrintFormatter) -> Data {
    let renderer = UIPrintPageRenderer()
    renderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
    let page = CGRect(x: 0, y: 0, width: 841.8, height: 595.2) // A4, 72 dpi
    renderer.setValue(page, forKey: "paperRect")
    renderer.setValue(page, forKey: "printableRect")
    let pdfData = NSMutableData()
    UIGraphicsBeginPDFContextToData(pdfData, page, nil)
    for i in 0..<renderer.numberOfPages {
      UIGraphicsBeginPDFPage()
      renderer.drawPage(at: i, in: UIGraphicsGetPDFContextBounds())
    }
    UIGraphicsEndPDFContext()
    do {
      try pdfData.write(to: url, options: .atomic)
      return pdfData as Data
    } catch {
      fatalError(error.localizedDescription)
    }
  }
  
  public var printFormatter: UIPrintFormatter!
  
  @discardableResult
  public func getPDF() -> Data {
    printFormatter = UIMarkupTextPrintFormatter(markupText: getHTML(from: "wab.html"))
    return createPDF(using: printFormatter)
  }
}
