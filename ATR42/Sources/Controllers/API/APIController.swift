//
//  APIController.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

struct APIController {
  
  static func getAircrafts(completion: @escaping (NetworkError?) -> Void) {
    
    guard let path = Bundle.main.path(forResource: "aircrafts", ofType: "json") else {
      completion(.network)
      return
    }
    
    guard let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe),
      let json = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any],
      let root = json else {
        completion(.encoding)
        return
    }
    
    if let aircrafts: [AircraftEntity] = try? JsonController.objectsFromJson(root, by: "aircraftsTypes") {
      StorageController().save(objects: aircrafts, update: true, completion: {
        if let tails: [AircraftTail] = try? JsonController.objectsFromJson(root, by: "aircrafts") {
          
          tails.filter { $0.aircraft != nil && $0.aircraft!.allowSecondAttendant }.forEach { tail in
            for crewCode in Set(tail.dods.flatMap {$0.crew}.filter {$0.cabin == 1}) {
              for dod in tail.dods.filter({ $0.crew == crewCode}) {
                guard let crew = dod.crew, let pantry = dod.pantry else { continue }
                let secondAttendantIndex = tail.secondAttendantSeatPerWeight * tail.aircraft!.secondAttendantWeight
                let newDod = AircraftDod(pantry: pantry,
                                         crew: AircraftCrewCode(cockpit: crew.cockpit, cabin: 2),
                                         dow: dod.dow + tail.aircraft!.secondAttendantWeight,
                                         doi: dod.doi + secondAttendantIndex)
                tail.dods.append(newDod)
              }
            }
          }
          
          StorageController().save(objects: tails, update: true, completion: nil)
        }
      })
    }
  }
}
