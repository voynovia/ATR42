//
//  NetworkError.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

enum NetworkError: Error {
  case encoding, network, server
  var localizedDescription: String {
    switch self {
    case .encoding:
      return "Используются недопустимые символы".localized
    case .network:
      return "Проверьте сетевое соединение".localized
    case .server:
      return "Ошибка сервера".localized
    }
  }
}
