//
//  Graph.swift
//  WaB
//
//  Created by Igor Voynov on 14.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class GraphController {
  
  private var weightMin: Int = Int.max
  private var weightMax: Int = Int.min
  private var indexMin: Int = Int.max
  private var indexMax: Int = Int.min
  private var macMin: Int = Int.max
  private var macMax: Int = Int.min
  private var xNum: Int = Int.min
  private var yNum: Int = Int.min
  
  private var weightScale: WeightScaleView!
  private var macScale: MacScaleView!
  private var indexScale: IndexScaleView!
  private var workspaceView: WorkspaceView!
  private var stabilizerView: StabilizerView!
  
  private let panelSize = CGSize(width: 30, height: 50)
  private var stabilizerHeight: CGFloat {
    return panelSize.height * 2
  }
  private var xCoord: CGFloat = 0
  private var yCoord: CGFloat = 0
  private var width: CGFloat = 0
  private var height: CGFloat = 0
  
  public init() {
    guard aircraft != nil else {
      assertionFailure("You must use the WaB class")
      return
    }
  }

  private func addWeightScale(at view: UIView) {
    weightScale?.removeFromSuperview()
    weightScale = WeightScaleView(frame: CGRect(x: xCoord,
                                                y: yCoord + panelSize.height,
                                                width: panelSize.width * 2,
                                                height: height - panelSize.height * 2 - stabilizerHeight))
    weightScale.minValue = weightMin / 1000
    weightScale.maxValue = weightMax / 1000
    view.addSubview(weightScale)
  }
  
  private func addMacScale(at view: UIView) {
    macScale?.removeFromSuperview()
    macScale = MacScaleView(frame: CGRect(x: xCoord + weightScale.frame.width,
                                          y: yCoord,
                                          width: width - weightScale.frame.width,
                                          height: panelSize.height))
    macScale.minValue = macMin
    macScale.maxValue = macMax
    view.addSubview(macScale)
  }
  
  private func addIndexScale(at view: UIView) {
    indexScale?.removeFromSuperview()
    indexScale = IndexScaleView(frame: CGRect(x: xCoord + weightScale.frame.width,
                                              y: yCoord + height - panelSize.height - stabilizerHeight,
                                              width: width - weightScale.frame.width,
                                              height: panelSize.height))
    indexScale.minValue = indexMin
    indexScale.maxValue = indexMax
    view.addSubview(indexScale)
  }
  
  private func addStabilizator(at view: UIView) {
    stabilizerView?.removeFromSuperview()
    stabilizerView = StabilizerView(min: aircraft.stabMin,
                                    max: aircraft.stabMax,
                                    frame: CGRect(x: xCoord, y: indexScale.frame.maxY,
                                                  width: width, height: stabilizerHeight))
    view.addSubview(stabilizerView)
  }
  
  private func addWorkspace(at view: UIView) {
    let workspace = WorkspaceView(frame: CGRect(x: xCoord + weightScale.frame.width,
                                                y: yCoord + panelSize.height,
                                                width: width - weightScale.frame.width,
                                                height: height - panelSize.height * 2 - stabilizerHeight))
    
    guard let mtowBox = aircraft.boxes.first(where: {$0.type == "mtow"}),
      let mlwBox = aircraft.boxes.first(where: {$0.type == "mlw"}),
      let mzfwBox = aircraft.boxes.first(where: {$0.type == "mzfw"}) else { return }
      
    self.workspaceView = workspace
    workspace.mtowBox = mtowBox
    workspace.mlwBox = mlwBox
    workspace.mzfwBox = mzfwBox
    workspace.dtowBox = aircraft.boxes.first(where: {$0.type == "dtow"})
    workspace.dlwBox = aircraft.boxes.first(where: {$0.type == "dlw"})
    workspace.dzfwBox = aircraft.boxes.first(where: {$0.type == "dzfw"})
    workspace.minX = indexMin
    workspace.maxX = indexMax
    workspace.minY = weightMin
    workspace.maxY = weightMax
    workspace.minTop = macMin
    workspace.maxTop = macMax
    view.addSubview(workspace)
  }
  
  public func draw(at view: UIView) {
    self.weightMin = Int.max
    self.weightMax = Int.min
    self.indexMin = Int.max
    self.indexMax = Int.min
    self.macMin = Int.max
    self.macMax = Int.min
    self.xNum = Int.min
    self.yNum = Int.min
    
    let rect = view.bounds
    
    // defenition of weight range
    var weightMin: Double = Double(self.weightMin)
    var weightMax: Double = Double(self.weightMax)
    for line in aircraft.boxes.flatMap({$0}) {
      if let max = line.aft.max(by: {$0.weight < $1.weight})?.weight, weightMax < max {
        weightMax = max
      }
      if let min = line.forward.max(by: {$0.weight > $1.weight})?.weight, weightMin > min {
        weightMin = min
      }
    }
    
    // defenition of index range
    var indexMin: Double = Double(self.indexMin)
    var indexMax: Double = Double(self.indexMax)
    for line in aircraft.boxes.flatMap({$0}) {
      if let max = line.aft.max(by: {$0.index < $1.index})?.index, indexMax < max {
        indexMax = max
      }
      if let min = line.forward.max(by: {$0.index > $1.index})?.index, indexMin > min {
        indexMin = min
      }
    }
    
    self.macMin = Int(floor(WABMath.prMAC(for: aircraft, index: indexMin, weight: weightMin)))
    self.macMax = Int(ceil(WABMath.prMAC(for: aircraft, index: indexMax, weight: weightMin)))
    self.weightMin = Int(floor(weightMin/1000) * 1000)
    self.weightMax = Int(ceil(weightMax/1000) * 1000)
    self.indexMin = Int(floor(indexMin / 10) * 10)
    self.indexMax = Int(ceil(indexMax / 10) * 10)
    self.yNum = (self.weightMax - self.weightMin) / 1000
    self.xNum = self.indexMax - self.indexMin + 5
    
    width = (floor((rect.width - panelSize.width * 2)
      / CGFloat(xNum)) * CGFloat(xNum))
      + panelSize.width * 2
    height = (floor((rect.height - panelSize.height * 2 - stabilizerHeight)
      / CGFloat(yNum)) * CGFloat(yNum))
      + panelSize.height * 2
      + stabilizerHeight
    xCoord = rect.minX + (rect.width - width) / 2
    yCoord = rect.minY + (rect.height - height) / 2
    
    addWeightScale(at: view)
    addMacScale(at: view)
    addIndexScale(at: view)
    addStabilizator(at: view)
    addWorkspace(at: view)
  }
  
  public func clean() {
    workspaceView?.clean()
    stabilizerView?.clean()
  }
  
  public func update(tPoint: Point, lPoint: Point, zPoint: Point, stabValue: Double) {
    workspaceView?.drawPoints(tPoint: tPoint, lPoint: lPoint, zPoint: zPoint)
    stabilizerView?.setValue(mac: tPoint.mac, weight: stabValue)
  }
}
