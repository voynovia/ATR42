//
//  Storage.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import RealmSwift

struct StorageSorted {
  var key: String
  var ascending: Bool = true
}

class StorageController {
  
  var realm: Realm!
  
  init() {
    let file = Realm.Configuration.defaultConfiguration.fileURL!
      .deletingLastPathComponent().appendingPathComponent("database.realm")
    let config = Realm.Configuration(fileURL: file,
                                     schemaVersion: 0,
                                     deleteRealmIfMigrationNeeded: true)
    self.realm = try! Realm(configuration: config) // swiftlint:disable:this force_try
  }
  
  deinit {
    if Thread.isMainThread == false {
      realm?.refresh()
    }
  }
  
  private func safeWrite(_ realm: Realm, block: (() throws -> Void)) throws {
    if realm.isInWriteTransaction {
      try block()
    } else {
      try realm.write(block)
    }
  }
  
  // MARK: - Write methods
  
  func delete(objects: [Object]) {
    autoreleasepool {
      guard let realm = realm else { return }
      try? self.safeWrite(realm) {
        realm.delete(objects)
      }
    }
  }
  
  func delete<T: Object>(_ type: T.Type, byPrimaryKey primaryKey: Any) {
    autoreleasepool {
      guard let realm = realm,
        let object = realm.object(ofType: type,
          forPrimaryKey: primaryKey) else { return }
      try? self.safeWrite(realm) {
        realm.delete(object)
      }
    }
  }
  
  func deleteAll<T: Object>(_ type: T.Type, completion: (() -> Void)?) {
    autoreleasepool {
      guard let realm = realm else { return }
      try? self.safeWrite(realm) {
        realm.delete(realm.objects(type))
      }
      completion?()
    }
  }
  
  func deleteAll(completion: (() -> Void)?) {
    autoreleasepool {
      guard let realm = realm else { return }
      try? self.safeWrite(realm) {
        realm.deleteAll()
      }
      completion?()
    }
  }
  
  func create<T: Object>(_ model: T.Type, completion: @escaping (T) -> Void) {
    autoreleasepool {
      guard let realm = realm else { return }
      try? self.safeWrite(realm) {
        let newObject = realm.create(model, value: [], update: true)
        completion(newObject)
      }
    }
  }
  
  func save(object: Object, update: Bool, completion: (() -> Void)?) {
    autoreleasepool {
      guard let realm = realm else { return }
      try? self.safeWrite(realm) {
        realm.add(object, update: update)
      }
      completion?()
    }
  }
  
  func save(objects: [Object], update: Bool, completion: (() -> Void)?) {
    guard objects.count > 0 else {
      completion?()
      return
    }
    autoreleasepool {
      guard let realm = realm else { return }
      try? self.safeWrite(realm) {
        realm.add(objects, update: update)
        completion?()
      }
    }
  }
  
  func update(block: @escaping () -> Void, completion: (() -> Void)?) {
    autoreleasepool {
      try? self.safeWrite(realm) {
        block()
      }
      completion?()
    }
  }
  
  func execute(block: @escaping (_ db: Realm) -> Void, completion: (() -> Void)?) {
    autoreleasepool {
      try? self.safeWrite(realm) {
        block(realm)
      }
      completion?()
    }
  }
  
  // MARK: - Read methods
  
  var isEmpty: Bool {
    guard let realm = realm else { return false }
    return realm.isEmpty
  }
  
  func fetch<T: Object>(_ type: T.Type, forPrimaryKey primaryKey: Any) -> T? {
    guard let realm = realm else { return nil }
    let object = realm.object(ofType: T.self, forPrimaryKey: primaryKey)
    return object
  }
  
  func fetchObjects<T: Object>(_ model: T.Type,
                               predicates: [NSPredicate]?,
                               sorted: StorageSorted?) -> Results<T> {
    var objects = realm.objects(model)
    if let predicates = predicates {
      objects = objects.filter(NSCompoundPredicate(andPredicateWithSubpredicates: predicates))
    }
    if let sorted = sorted {
      objects = objects.sorted(byKeyPath: sorted.key, ascending: sorted.ascending)
    }
    return objects
  }
}
