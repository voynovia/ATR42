//
//  AppDelegate.swift
//  ATR42
//
//  Created by Igor Voynov on 19.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    AppearanceController.setupAppearance()
    APIController.getAircrafts { error in
      print("get aircafts:", error?.localizedDescription ?? "ok")
    }
    
    return true
  }
}
