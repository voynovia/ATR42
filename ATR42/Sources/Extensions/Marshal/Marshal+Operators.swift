//
//  Marshal+Operators.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Marshal

func <| <A: ValueType>(left: inout [String: Any], right: (key: String, value: A?)) {
  if let value = right.value {
    left[right.key] = value
  }
}

func <| <A: ValueType>(left: inout [String: Any], right: (key: String, values: [A]?)) {
  if let values = right.values {
    left[right.key] = values
  }
}

func <| <A: ValueType>(left: inout [String: Any], right: (key: String, value: A?)) where A: Marshaling {
  if let value = right.value {
    left[right.key] = value.marshaled()
  }
}

func <| <A: ValueType>(left: inout [String: Any], right: (key: String, values: [A]?)) where A: Marshaling {
  if let values = right.values {
    left[right.key] = values.map { $0.marshaled() }
  }
}

func <| <A: ValueType>(left: inout [String: Any], right: (key: String, value: [String: A]?)) {
  if let value = right.value {
    left[right.key] = value
  }
}

func <| <A: ValueType>(left: inout [String: Any], right: (key: String, values: [[String: A]]?)) {
  if let values = right.values {
    left[right.key] = values
  }
}

func <| <A: RawRepresentable>(left: inout [String: Any], right: (key: String, value: A?)) where A.RawValue: ValueType {
  if let value = right.value {
    left[right.key] = value.rawValue
  }
}

func <| <A: RawRepresentable>(left: inout [String: Any], right: (key: String, values: [A]?)) where A.RawValue: ValueType {
  if let values = right.values {
    left[right.key] = values.map { $0.rawValue }
  }
}
