//
//  Marshal+Realm.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import RealmSwift
import Marshal

func <| <A: Marshaling>(left: inout [String: Any], right: (key: String, values: List<A>?)) {
  if let values = right.values {
    left[right.key] = Array(values).map { $0.marshaled() }
  }
}

func <| <A: ValueType>(dictionary: MarshaledObject, key: String) -> List<A> {
  return dictionary.value(for: key)
}

extension MarshaledObject {
  func value<A: ValueType>(for key: KeyType) -> List<A> {
    guard let any = try? self.any(for: key) else {
      return List<A>()
    }
    return List<A>.value(from: any)
  }
}

extension List where Element: ValueType {
  public static func value<T: ValueType>(from object: Any) -> List<T> {
    guard let anyArray = object as? [Any] else {
      #if DEBUG
      print(MarshalError.typeMismatch(expected: self, actual: type(of: object)))
      #endif
      return List<T>()
    }
    let array: [T] = anyArray.flatMap {
      let value = try? Element.value(from: $0)
      guard let element = value as? Element else {
        return nil
      }
      return element as? T
    }
    let list = List<T>()
    list.append(objectsIn: array)
    return list
  }
}
