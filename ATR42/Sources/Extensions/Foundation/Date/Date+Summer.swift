//
//  Date+Summer.swift
//  ATR42
//
//  Created by Igor Voynov on 04.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

extension Date {
  var isSummer: Bool {
    func lastSunday(of month: Int) -> Date {
      let calendar = Calendar.autoupdatingCurrent
      var dateComponents = DateComponents(calendar: calendar,
                                          year: calendar.component(.year, from: self),
                                          month: month + 1,
                                          day: 0)
      let date = calendar.date(from: dateComponents)!
      let weekday = calendar.component(.weekday, from: date)
      if weekday != 1 {
        dateComponents.day! -= weekday - 1
      }
      return calendar.date(from: dateComponents)!
    }
    return self >= lastSunday(of: 3) && self < lastSunday(of: 10) ? true : false
  }
}
