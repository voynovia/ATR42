//
//  Error+App.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

extension Error {
  var code: Int { return (self as NSError).code }
  var domain: String { return (self as NSError).domain }
}

extension NSError {
  convenience init(_ error: Error, file: String = #file) {
    self.init(domain: URL(fileURLWithPath: file).deletingPathExtension().lastPathComponent,
              code: error.code,
              userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(error.localizedDescription, comment: "")])
  }
  convenience init(_ description: String, code: Int, file: String = #file) {
    self.init(domain: URL(fileURLWithPath: file).deletingPathExtension().lastPathComponent,
              code: code,
              userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(description, comment: "")])
  }
}
