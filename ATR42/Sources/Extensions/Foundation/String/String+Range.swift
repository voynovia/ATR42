//
//  String+Range.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

extension String {
  func substring(from index: Int) -> String {
    let index = self.index(self.startIndex, offsetBy: index)
    return String(self[index...])
  }
  
  func substring(with range: Range<Int>) -> String {
    let startIndex = self.index(self.startIndex, offsetBy: range.lowerBound)
    let endIndex = self.index(self.startIndex, offsetBy: range.upperBound)
    return String(self[startIndex..<endIndex])
  }
}
