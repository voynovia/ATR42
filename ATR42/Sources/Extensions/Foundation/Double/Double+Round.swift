//
//  Double+Round.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

public extension Double {
  func rounded(toPlaces places: Int) -> Double {
    let divisor = pow(10.0, Double(places))
    return (self * divisor).rounded() / divisor
  }
}
