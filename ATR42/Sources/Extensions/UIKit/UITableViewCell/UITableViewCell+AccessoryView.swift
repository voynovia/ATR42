//
//  UITableViewCell+AccessoryView.swift
//  Vista
//
//  Created by Igor Voynov on 12.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit.UITableViewCell

extension UITableViewCell {
  func setAccessoryViews(_ views: [UIView], distance: CGFloat) {
    accessoryView = UIView(frame: self.frame)
    var x: CGFloat = 0
    for view in views {
      x += distance
      let y = (frame.height - view.intrinsicContentSize.height) / 2
      view.frame = CGRect(x: x, y: y, width: view.intrinsicContentSize.width, height: view.intrinsicContentSize.height)
      accessoryView?.addSubview(view)
      x += view.frame.width
    }
    accessoryView?.frame = CGRect(x: 0, y: 0, width: x, height: frame.height)
  }
}
