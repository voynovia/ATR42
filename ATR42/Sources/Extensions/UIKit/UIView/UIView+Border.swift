//
//  UIView+Border.swift
//  ATR42
//
//  Created by Igor Voynov on 14.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

extension UIView {
  func addBottomBorderWithColor(_ color: UIColor, width: CGFloat) {
    let path = UIBezierPath()
    path.lineWidth = width
    path.move(to: CGPoint(x: 0, y: self.bounds.height - width))
    path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height - width))
    let borderLayer = CAShapeLayer()
    borderLayer.path = path.cgPath
    borderLayer.fillColor = color.cgColor
    borderLayer.strokeColor = color.cgColor
    borderLayer.lineWidth = width
    layer.addSublayer(borderLayer)
  }
}
