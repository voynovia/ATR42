//
//  UIColor.swift
//  ATR42
//
//  Created by Igor Voynov on 27.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit.UIColor

extension UIColor {
  enum ColorType {
    case backgroundColor
    case backgroundGraph
  }
  
  convenience init(_ colorType: ColorType) {
    switch colorType {
    case .backgroundColor: self.init(hexString: "3A3A3C")
    case .backgroundGraph: self.init(hexString: "D1D3D6")
    }
  }
}
