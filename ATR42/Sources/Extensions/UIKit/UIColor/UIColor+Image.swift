//
//  UIColor+Image.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit

extension UIColor {
  func as1ptImage() -> UIImage {
    UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
    setFill()
    UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
    let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    UIGraphicsEndImageContext()
    return image
  }
}
