//
//  UILabel+Underline.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit.UILabel

public extension UILabel {
  func underLine() {
    if let textUnwrapped = self.text {
      let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
      let underlineAttributedString = NSAttributedString(string: textUnwrapped, attributes: underlineAttribute)
      self.attributedText = underlineAttributedString
    }
  }
}
