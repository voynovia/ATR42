//
//  UITableView+Configure.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit.UITableView
import ObjectiveC

private var associationKey = "tableView_bottom_inset"

struct TableViewConfiguration {
  var topInset: CGFloat?
  var bottomInset: CGFloat?
  var bottomIndicatorInset: CGFloat?
  var estimatedRowHeight: CGFloat?
  var rowHeight: CGFloat?
  var allowsSelection: Bool!

  init(
    topInset: CGFloat? = nil,
    bottomInset: CGFloat? = nil,
    bottomIndicatorInset: CGFloat? = nil,
    estimatedRowHeight: CGFloat? = nil,
    rowHeight: CGFloat? = nil,
    allowsSelection: Bool = true
    ) {
    self.topInset = topInset
    self.bottomInset = bottomInset
    self.bottomIndicatorInset = bottomIndicatorInset
    self.estimatedRowHeight = estimatedRowHeight
    self.rowHeight = rowHeight
    self.allowsSelection = allowsSelection
  }
  
  static var `default` = defaultConfiguration
}

private var defaultConfiguration: TableViewConfiguration {
  return TableViewConfiguration(
    rowHeight: UITableViewAutomaticDimension,
    allowsSelection: true
  )
}

extension UITableView {
  
  private(set) var defaultBottomInset: CGFloat {
    get {
      return (objc_getAssociatedObject(self, &associationKey) as? CGFloat) ?? defaultConfiguration.bottomInset!
    }
    set {
      objc_setAssociatedObject(self, &associationKey, newValue, .OBJC_ASSOCIATION_RETAIN)
    }
  }
  
  enum ConfigurationType {
    case defaultConfiguration
    case custom(TableViewConfiguration)
  }
  
  func configure(with configuration: ConfigurationType) {
    switch configuration {
    case .defaultConfiguration:
      setup(configuration: defaultConfiguration)
    case let .custom(customConfig):
      setup(configuration: customConfig)
    }
  }
  
  private func setup(configuration: TableViewConfiguration) {
    if let topInset = configuration.topInset {
      self.contentInset.top = topInset
    }
    if let bottomInset = configuration.bottomInset {
      self.contentInset.bottom = bottomInset
      defaultBottomInset = bottomInset
    }
    if let bottomIndicatorInset = configuration.bottomIndicatorInset {
      self.scrollIndicatorInsets.bottom = bottomIndicatorInset
    }
    if let estimated = configuration.estimatedRowHeight {
      self.estimatedRowHeight = estimated
    }
    if let rowHeight = configuration.rowHeight {
      self.rowHeight = rowHeight
    }
    self.allowsSelection = configuration.allowsSelection
  }
  
}
