//
//  UITableView+ShakeRow.swift
//  Vista
//
//  Created by Igor Voynov on 12.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit
import AudioToolbox

extension UITableView {
  private func shakeRow(_ indexPath: IndexPath) {
    let cell = self.cellForRow(at: indexPath)
    cell?.shake()
  }
  
  func failedShakeRow(_ indexPath: IndexPath) {
    self.shakeRow(indexPath)
    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
  }
}
