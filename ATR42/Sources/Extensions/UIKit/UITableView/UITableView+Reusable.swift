//
//  UITableView+Reusable.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit.UITableView

extension UITableView {
  func dequeueReusableCell(for indexPath: IndexPath, with model: CellViewAnyModelType) -> UITableViewCell {
    let cellIdentifier = String(describing: type(of: model).cellClass())
    let cell = dequeueReusableCell(withIdentifier: cellIdentifier,
                                   for: indexPath)
    model.setupDefault(on: cell)
    return cell
  }
}

protocol ReusableCell {
  static var identifier: String { get }
  static var nib: UINib { get }
}

extension UITableViewCell: ReusableCell {
  static var identifier: String {
    return String(describing: self)
  }
  
  static var nib: UINib {
    return UINib(nibName: identifier, bundle: nil)
  }
}

extension UITableViewHeaderFooterView: ReusableCell {
  static var identifier: String {
    return String(describing: self)
  }
  
  static var nib: UINib {
    return UINib(nibName: identifier, bundle: nil)
  }
}
