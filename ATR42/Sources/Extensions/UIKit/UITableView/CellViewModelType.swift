//
//  CellViewModelType.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit.UITableView

protocol CellViewAnyModelType {
  static func cellClass() -> UIView.Type
  func setupDefault(on cell: UIView)
  
  var canEdit: Bool { get }
  var editStyle: UITableViewCellEditingStyle { get }
  var isValid: Bool { get }
}

protocol CellViewModelType: CellViewAnyModelType {
  associatedtype CellClass: UIView
  func setup(on cell: CellClass)
  
  var canEdit: Bool { get }
  var editStyle: UITableViewCellEditingStyle { get }
  var isValid: Bool { get }
}

// From generic to runtime
extension CellViewModelType {
  static func cellClass() -> UIView.Type {
    return Self.CellClass.self
  }
  
  func setupDefault(on cell: UIView) {
    setup(on: cell as! Self.CellClass) // swiftlint:disable:this force_cast
  }
  
  var canEdit: Bool {
    switch editStyle {
    case .delete, .insert:
      return true
    case .none:
      return false
    }
  }
  var editStyle: UITableViewCellEditingStyle {
    return .none
  }
  var isValid: Bool {
    return true
  }
}
