//
//  DisplayCollection.swift
//  Vista
//
//  Created by Igor Voynov on 02.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit
import SafariServices

protocol DisplayCollection: class {
  var numberOfSections: Int { get }
  static var modelsForRegistration: [CellViewAnyModelType.Type] { get }
  
  func numberOfRows(in section: Int) -> Int
  func model(for indexPath: IndexPath) -> CellViewAnyModelType
}

protocol DisplayCollectionAction {
  func didSelect(indexPath: IndexPath)
}

extension DisplayCollection {
  var numberOfSections: Int {
    return 1
  }
}

protocol DisplayCollectionDelegate: class {
  func registerNibs(from displayCollecton: DisplayCollection)
  
  func updateUI()
  func updateRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation)
  func updateVisibleRows(with animation: UITableViewRowAnimation)
  func updateSections(_ indexSet: IndexSet, with animation: UITableViewRowAnimation)
  
  func present(viewController: UIViewController)
  func dissmisSelf(animated: Bool, completion: (() -> Void)?)
  func pop()
  func push(viewController: UIViewController)
  func open(url: URL)
  
  func showDetail(viewController: UIViewController)
  
  var isUserInteractionEnabled: Bool { get set }
}

extension UIViewController: DisplayCollectionDelegate {
  
  func registerNibs(from displayCollecton: DisplayCollection) {
    if let tableView = self.value(forKey: "tableView") as? UITableView {
      tableView.registerNibs(from: displayCollecton)
    }
  }
  
  func updateUI() {
    if let tableView = self.value(forKey: "tableView") as? UITableView {
      tableView.reloadData()
    }
  }
  func updateRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
    if let tableView = self.value(forKey: "tableView") as? UITableView {
      tableView.reloadRows(at: indexPaths, with: animation)
    }
  }
  func updateVisibleRows(with animation: UITableViewRowAnimation) {
    if let tableView = self.value(forKey: "tableView") as? UITableView,
      let indexPaths = tableView.indexPathsForVisibleRows {
      tableView.reloadRows(at: indexPaths, with: animation)
    }
  }
  func updateSections(_ indexSet: IndexSet, with animation: UITableViewRowAnimation) {
    if let tableView = self.value(forKey: "tableView") as? UITableView {
      tableView.reloadSections(indexSet, with: animation)
    }
  }
  
  var isUserInteractionEnabled: Bool {
    get {
      if let tableView = self.value(forKey: "tableView") as? UITableView {
        return tableView.isUserInteractionEnabled
      } else {
        return true
      }
    }
    set {
      if let tableView = self.value(forKey: "tableView") as? UITableView {
        tableView.isUserInteractionEnabled = isUserInteractionEnabled
      }
    }
  }
  
  func present(viewController: UIViewController) {
    present(viewController, animated: true, completion: nil)
  }
  
  func pop() {
    navigationController?.popViewController(animated: true)
  }
  
  func dissmisSelf(animated: Bool, completion: (() -> Void)?) {
    dismiss(animated: animated, completion: completion)
  }
  
  func push(viewController: UIViewController) {
    navigationController?.pushViewController(viewController, animated: true)
  }
  
  func open(url: URL) {
    let safariViewController = SFSafariViewController(url: url)
    present(viewController: safariViewController)
  }
  
  func showDetail(viewController: UIViewController) {
    splitViewController?.showDetailViewController(viewController, sender: self)
  }
}
