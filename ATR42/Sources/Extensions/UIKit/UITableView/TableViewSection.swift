//
//  TableViewSection.swift
//  ATR42
//
//  Created by Igor Voynov on 23.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import Foundation

protocol TableViewSectionViewModelType {
  var titleForHeader: String? { get }
  var titleForFooter: String? { get }
  var rows: [CellViewAnyModelType] { get set }
}

class TableViewSection: TableViewSectionViewModelType {
  var titleForHeader: String?
  var titleForFooter: String?
  var rows: [CellViewAnyModelType] = []
  
  init(titleForHeader: String?, titleForFooter: String?, rows: [CellViewAnyModelType]) {
    self.titleForHeader = titleForHeader
    self.titleForFooter = titleForFooter
    self.rows = rows
  }
}
