//
//  UIImage+Save.swift
//  ATR42
//
//  Created by Igor Voynov on 29.01.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit.UIImage

public extension UIImage {
  enum SaveError: Error {
    case JPEGRepresentation
  }
  
  public func save(to url: URL) throws {
    guard let data = UIImageJPEGRepresentation(self, 0.8) else {
      throw SaveError.JPEGRepresentation
    }
    try data.write(to: url)
  }
}
