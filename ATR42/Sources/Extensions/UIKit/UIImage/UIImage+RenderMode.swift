//
//  UIImage+RenderMode.swift
//  ATR42
//
//  Created by Igor Voynov on 06.03.2018.
//  Copyright © 2018 Igor Voynov. All rights reserved.
//

import UIKit.UIImage

extension UIImage {
  var imageWithTemplateRendingMode: UIImage {
    return self.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
  }
}
